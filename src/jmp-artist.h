/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef _JMP_ARTIST
#define _JMP_ARTIST

#include <glib-object.h>

G_BEGIN_DECLS

#define JMP_ARTIST_TYPE jmp_artist_get_type()
#define JMP_ARTIST(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), JMP_ARTIST_TYPE, JmpArtist))
#define JMP_ARTIST_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), JMP_ARTIST_TYPE, JmpArtistClass))
#define IS_JMP_ARTIST(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JMP_ARTIST_TYPE))
#define IS_JMP_ARTIST_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), JMP_ARTIST_TYPE))
#define JMP_ARTIST_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), JMP_ARTIST_TYPE, JmpArtistClass))

typedef struct _JmpArtistPrivate JmpArtistPrivate;

typedef struct {
        GObject parent;

        /* <private> */
        JmpArtistPrivate *priv;
} JmpArtist;

typedef struct {
        GObjectClass parent_class;
} JmpArtistClass;

GType jmp_artist_get_type (void);
JmpArtist* jmp_artist_new (void);

void jmp_artist_set_name (JmpArtist *self, const gchar *name);
void jmp_artist_set_genre (JmpArtist *self, const gchar *genre);
void jmp_artist_set_id (JmpArtist *self, const char *id);
void jmp_artist_set_image (JmpArtist *self, const gchar *image);
void jmp_artist_set_idstr (JmpArtist *self, const gchar *idstr);
void jmp_artist_set_mbid (JmpArtist *self, const gchar *mbid);
void jmp_artist_set_mbgid (JmpArtist *self, const gchar *mbgid);
void jmp_artist_set_url (JmpArtist *self, const gchar *url);

G_END_DECLS

#endif /*_JMP_ARTIST */
