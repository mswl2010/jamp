/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "jmp-artist.h"

G_DEFINE_TYPE (JmpArtist, jmp_artist, G_TYPE_OBJECT)

enum {
        PROP_0,
        PROP_NAME,
        PROP_ID,
        PROP_IDSTR,
        PROP_IMAGE,
        PROP_GENRE,
        PROP_MBGID,
        PROP_MBID,
        PROP_URL
};

#define GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), JMP_ARTIST_TYPE, JmpArtistPrivate))

struct _JmpArtistPrivate {
        gchar *name;
        gchar *id;
        gchar *idstr;
        gchar *image;
        gchar *genre;
        gchar *mbgid;
        gchar *mbid;
        gchar *url;
};

static void
jmp_artist_get_property (GObject *object, guint property_id,
                         GValue *value, GParamSpec *pspec)
{
        JmpArtist *self = JMP_ARTIST (object);

        switch (property_id) {
        case PROP_NAME:
                g_value_set_string (value, self->priv->name);
                break;
        case PROP_ID:
                g_value_set_string (value, self->priv->id);
                break;
        case PROP_IDSTR:
                g_value_set_string (value, self->priv->idstr);
                break;
        case PROP_IMAGE:
                g_value_set_string (value, self->priv->image);
                break;
        case PROP_GENRE:
                g_value_set_string (value, self->priv->genre);
                break;
        case PROP_MBGID:
                g_value_set_string (value, self->priv->mbgid);
                break;
        case PROP_MBID:
                g_value_set_string (value, self->priv->mbid);
                break;
        case PROP_URL:
                g_value_set_string (value, self->priv->url);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
jmp_artist_set_property (GObject *object, guint property_id,
                         const GValue *value, GParamSpec *pspec)
{
        JmpArtist *self = JMP_ARTIST (object);

        switch (property_id) {
        case PROP_NAME:
                jmp_artist_set_name (self, g_value_get_string (value));
                break;
        case PROP_ID:
                jmp_artist_set_id (self, g_value_get_string (value));
                break;
        case PROP_IDSTR:
                jmp_artist_set_idstr (self, g_value_get_string (value));
                break;
        case PROP_IMAGE:
                jmp_artist_set_image (self, g_value_get_string (value));
                break;
        case PROP_GENRE:
                jmp_artist_set_genre (self, g_value_get_string (value));
                break;
        case PROP_MBGID:
                jmp_artist_set_mbgid (self, g_value_get_string (value));
                break;
        case PROP_MBID:
                jmp_artist_set_mbid (self, g_value_get_string (value));
                break;
        case PROP_URL:
                jmp_artist_set_url (self, g_value_get_string (value));
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
jmp_artist_finalize (GObject *object)
{
        JmpArtist *self = JMP_ARTIST (object);
        g_free (self->priv->name);
        g_free (self->priv->id);
        g_free (self->priv->idstr);
        g_free (self->priv->image);
        g_free (self->priv->genre);
        g_free (self->priv->mbgid);
        g_free (self->priv->mbid);
        g_free (self->priv->url);

        G_OBJECT_CLASS (jmp_artist_parent_class)->finalize (object);
}

static void
jmp_artist_class_init (JmpArtistClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        g_type_class_add_private (klass, sizeof (JmpArtistPrivate));

        object_class->get_property = jmp_artist_get_property;
        object_class->set_property = jmp_artist_set_property;
        object_class->finalize = jmp_artist_finalize;

        g_object_class_install_property
                (object_class, PROP_NAME,
                 g_param_spec_string ("name", "Artist name", "The name of the artist",
                                      NULL,
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_ID,
                 g_param_spec_string ("id", "Artist numeric id",
                                      "The numeric identifier of the artist",
                                      NULL,
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_IDSTR,
                 g_param_spec_string ("idstr", "Artist string id",
                                      "The string identifier of the artist",
                                      NULL,
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_IMAGE,
                 g_param_spec_string ("image", "Image url",
                                      "Url pointing to the artist's image",
                                      NULL,
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_GENRE,
                 g_param_spec_string ("genre", "Genre",
                                      "Genre(s) of the artist",
                                      NULL,
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_MBGID,
                 g_param_spec_string ("mbgid", "Artist string id on MusicBrainz",
                                      "The string identifier of the artist on MusicBrainz",
                                      NULL,
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_MBID,
                 g_param_spec_string ("mbid", "Artist numeric id on MusicBrainz",
                                      "The numeric identifier of the artist on MusicBrainz",
                                      NULL,
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_URL,
                 g_param_spec_string ("url", "Artist url",
                                      "The url of the artist page on Jamendo",
                                      NULL,
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

}

static void
jmp_artist_init (JmpArtist *self)
{
        self->priv = GET_PRIVATE (self);
}

JmpArtist*
jmp_artist_new (void)
{
        return g_object_new (JMP_ARTIST_TYPE, NULL);
}

void
jmp_artist_set_name (JmpArtist *self, const gchar *name)
{
        g_free (self->priv->name);

        self->priv->name = g_strdup (name);
}

void
jmp_artist_set_id (JmpArtist *self, const gchar *id)
{
        g_free (self->priv->id);

        self->priv->id = g_strdup (id);
}

void
jmp_artist_set_idstr (JmpArtist *self, const gchar *idstr)
{
        g_free (self->priv->idstr);

        self->priv->idstr = g_strdup (idstr);
}

void
jmp_artist_set_image (JmpArtist *self, const gchar *image)
{
        g_free (self->priv->image);

        self->priv->image = g_strdup (image);
}

void
jmp_artist_set_genre (JmpArtist *self, const gchar *genre)
{
        g_free (self->priv->genre);

        self->priv->genre = g_strdup (genre);
}

void
jmp_artist_set_mbgid (JmpArtist *self, const gchar *mbgid)
{
        g_free (self->priv->mbgid);

        self->priv->mbgid = g_strdup (mbgid);
}

void
jmp_artist_set_mbid (JmpArtist *self, const gchar *mbid)
{
        g_free (self->priv->mbid);

        self->priv->mbid = g_strdup (mbid);
}

void
jmp_artist_set_url (JmpArtist *self, const gchar *url)
{
        g_free (self->priv->url);

        self->priv->url = g_strdup (url);
}
