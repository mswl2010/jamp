/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef _JMP_DATA_PROVIDER_SERVICE
#define _JMP_DATA_PROVIDER_SERVICE

#include <glib-object.h>

G_BEGIN_DECLS

#define JMP_DATA_PROVIDER_SERVICE_TYPE jmp_data_provider_service_get_type()
#define JMP_DATA_PROVIDER_SERVICE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), JMP_DATA_PROVIDER_SERVICE_TYPE, JmpDataProviderService))
#define JMP_DATA_PROVIDER_SERVICE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), JMP_DATA_PROVIDER_SERVICE_TYPE, JmpDataProviderServiceClass))
#define JMP_DATA_PROVIDER_IS_SERVICE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JMP_DATA_PROVIDER_SERVICE_TYPE))
#define JMP_DATA_PROVIDER_IS_SERVICE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), JMP_DATA_PROVIDER_SERVICE_TYPE))
#define JMP_DATA_PROVIDER_SERVICE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), JMP_DATA_PROVIDER_SERVICE_TYPE, JmpDataProviderServiceClass))

typedef struct _JmpDataProviderServicePrivate JmpDataProviderServicePrivate;

typedef struct {
        GObject parent;

        /* <private> */
        JmpDataProviderServicePrivate *priv;
} JmpDataProviderService;

typedef struct {
        GObjectClass parent_class;

        /* <private> */
        void (*response_received) (JmpDataProviderService *self, GList *list);
} JmpDataProviderServiceClass;

typedef struct struct_track {
        gchar *id;
        gchar *name;
        guint duration;
        gchar *url;
        gchar *stream;
} track;

GType jmp_data_provider_service_get_type (void);
JmpDataProviderService* jmp_data_provider_service_new (DBusGConnection *connection);

G_END_DECLS

#endif
