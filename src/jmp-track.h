/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef _JMP_TRACK
#define _JMP_TRACK

#include <glib-object.h>

G_BEGIN_DECLS

#define JMP_TRACK_TYPE jmp_track_get_type()
#define JMP_TRACK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), JMP_TRACK_TYPE, JmpTrack))
#define JMP_TRACK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), JMP_TRACK_TYPE, JmpTrackClass))
#define IS_JMP_TRACK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JMP_TRACK_TYPE))
#define IS_JMP_TRACK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), JMP_TRACK_TYPE))
#define JMP_TRACK_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), JMP_TRACK_TYPE, JmpTrackClass))

typedef struct _JmpTrackPrivate JmpTrackPrivate;

typedef struct {
        GObject parent;

        /* <private> */
        JmpTrackPrivate *priv;
} JmpTrack;

typedef struct {
        GObjectClass parent_class;
} JmpTrackClass;

GType jmp_track_get_type (void);
JmpTrack* jmp_track_new (void);

void jmp_track_set_id (JmpTrack *self, const gchar *id);
void jmp_track_set_name (JmpTrack *self, const gchar *name);
void jmp_track_set_duration (JmpTrack *self, const guint duration);
void jmp_track_set_stream (JmpTrack *self, const gchar *stream);
void jmp_track_set_url (JmpTrack *self, const gchar *url);





G_END_DECLS

#endif
