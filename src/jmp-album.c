/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "jmp-album.h"
#include "jmp-artist.h"

G_DEFINE_TYPE (JmpAlbum, jmp_album, G_TYPE_OBJECT)

enum {
        PROP_0,
        PROP_ID,
        PROP_NAME,
        PROP_IMAGE,
        PROP_URL,
        PROP_DURATION,
        PROP_GENRE,
        PROP_DATES,
        PROP_ARTIST
};

#define GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), JMP_ALBUM_TYPE, JmpAlbumPrivate))

struct _JmpAlbumPrivate {
        gchar *id;
        gchar *name;
        gchar *image;
        gchar *url;
        gchar *duration;
        gchar *genre;
        gchar *dates;
        JmpArtist *artist;
};

static void
jmp_album_get_property (GObject *object, guint property_id,
                         GValue *value, GParamSpec *pspec)
{
        JmpAlbum *self = JMP_ALBUM (object);

        switch (property_id) {
        case PROP_ID:
                g_value_set_string (value, self->priv->id);
                break;
        case PROP_NAME:
                g_value_set_string (value, self->priv->name);
                break;
        case PROP_IMAGE:
                g_value_set_string (value, self->priv->image);
                break;
        case PROP_URL:
                g_value_set_string (value, self->priv->url);
                break;
        case PROP_DURATION:
                g_value_set_string (value, self->priv->duration);
                break;
        case PROP_GENRE:
                g_value_set_string (value, self->priv->genre);
                break;
        case PROP_DATES:
                g_value_set_string (value, self->priv->dates);
                break;
        case PROP_ARTIST:
                g_value_set_pointer (value, self->priv->artist);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}


static void
jmp_album_set_property (GObject *object, guint property_id,
                         const GValue *value, GParamSpec *pspec)
{
        JmpAlbum *self = JMP_ALBUM (object);

        switch (property_id) {
        case PROP_ID:
                jmp_album_set_id (self, g_value_get_string (value));
                break;
        case PROP_NAME:
                jmp_album_set_name (self, g_value_get_string (value));
                break;
        case PROP_IMAGE:
                jmp_album_set_image (self, g_value_get_string (value));
                break;
        case PROP_URL:
                jmp_album_set_url (self, g_value_get_string (value));
                break;
        case PROP_DURATION:
                jmp_album_set_duration (self, g_value_get_string (value));
                break;
        case PROP_GENRE:
                jmp_album_set_genre (self, g_value_get_string (value));
                break;
        case PROP_DATES:
                jmp_album_set_dates (self, g_value_get_string (value));
                break;
        case PROP_ARTIST:
                jmp_album_set_artist (self, g_value_get_pointer (value));
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
jmp_album_finalize (GObject *object)
{
        JmpAlbum *self = JMP_ALBUM (object);
        g_free (self->priv->id);
        g_free (self->priv->name);
        g_free (self->priv->image);
        g_free (self->priv->url);
        g_free (self->priv->duration);
        g_free (self->priv->genre);
        g_free (self->priv->dates);
        g_object_unref (self->priv->artist);
        G_OBJECT_CLASS (jmp_album_parent_class)->finalize (object);
}

static void
jmp_album_class_init (JmpAlbumClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        g_type_class_add_private (klass, sizeof (JmpAlbumClass));

        object_class->get_property = jmp_album_get_property;
        object_class->set_property = jmp_album_set_property;
        object_class->finalize = jmp_album_finalize;

        g_object_class_install_property
                (object_class, PROP_ID,
                 g_param_spec_string ("id", "Album id", "Numeric id of the album",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_NAME,
                 g_param_spec_string ("name", "Album name",
                                      "Name of the album",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_IMAGE,
                 g_param_spec_string ("image", "Album image",
                                      "Link to the cover of the album",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_URL,
                 g_param_spec_string ("url", "Album url",
                                      "Link to the page of the album on Jamendo",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_DURATION,
                 g_param_spec_string ("duration", "Album duration",
                                      "Total length of the album in seconds",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_GENRE,
                 g_param_spec_string ("genre", "Album genre",
                                      "Description of the album written by the artist",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_DATES,
                 g_param_spec_string ("dates", "Album dates",
                                      "Release dates of the album",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_ARTIST,
                 g_param_spec_pointer ("artist", "Album artist",
                                      "Data about the author of the album",
                                      G_PARAM_READWRITE));
}

static void
jmp_album_init (JmpAlbum *self)
{
        self->priv = GET_PRIVATE (self);
        self->priv->artist = jmp_artist_new ();
}

JmpAlbum*
jmp_album_new (void)
{
        return g_object_new (JMP_ALBUM_TYPE, NULL);
}

void
jmp_album_set_id (JmpAlbum *self, const gchar *id)
{
        g_free (self->priv->id);

        self->priv->id = g_strdup (id);
}

void
jmp_album_set_name (JmpAlbum *self, const gchar *name)
{
        g_free (self->priv->name);

        self->priv->name = g_strdup (name);
}

void
jmp_album_set_image (JmpAlbum *self, const gchar *image)
{
        g_free (self->priv->image);

        self->priv->image = g_strdup (image);
}

void
jmp_album_set_url (JmpAlbum *self, const gchar *url)
{
        g_free (self->priv->url);

        self->priv->url = g_strdup (url);
}

void
jmp_album_set_duration (JmpAlbum *self, const gchar *duration)
{
        g_free (self->priv->duration);

        self->priv->duration = g_strdup (duration);
}

void
jmp_album_set_genre (JmpAlbum *self, const gchar *genre)
{
        g_free (self->priv->genre);

        self->priv->genre = g_strdup (genre);
}

void
jmp_album_set_dates (JmpAlbum *self, const gchar *dates)
{
        g_free (self->priv->dates);

        self->priv->dates = g_strdup (dates);
}

void
jmp_album_set_artist (JmpAlbum *self, JmpArtist  *artist)
{
        *(self->priv->artist) = *artist;
}
