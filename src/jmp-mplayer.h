/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef _JMP_MPLAYER
#define _JMP_MPLAYER

#include <glib-object.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#define JMP_TYPE_MPLAYER jmp_mplayer_get_type()
#define JMP_MPLAYER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), JMP_TYPE_MPLAYER, JmpMplayer))
#define JMP_MPLAYER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), JMP_TYPE_MPLAYER, JmpMplayerClass))
#define JMP_IS_MPLAYER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JMP_TYPE_MPLAYER))
#define JMP_IS_MPLAYER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), JMP_TYPE_MPLAYER))
#define JMP_MPLAYER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), JMP_TYPE_MPLAYER, JmpMplayerClass))

typedef struct _JmpMplayerPrivate JmpMplayerPrivate;

typedef struct {
        GObject parent;

        /* <private> */
        JmpMplayerPrivate *priv;
} JmpMplayer;

typedef struct {
        GObjectClass parent_class;
} JmpMplayerClass;

GType jmp_mplayer_get_type (void);
JmpMplayer* jmp_mplayer_new (void);

gboolean jmp_mplayer_set_uri (JmpMplayer *self, const gchar *uri);
gboolean jmp_mplayer_play (JmpMplayer *self);
gboolean jmp_mplayer_stop (JmpMplayer *self);
gboolean jmp_mplayer_pause (JmpMplayer *self);
gboolean jmp_mplayer_set_volume (JmpMplayer *self, gdouble volume);
gdouble jmp_mplayer_get_volume (JmpMplayer *self);
gboolean jmp_mplayer_toggle_mute (JmpMplayer *self);
gboolean jmp_mplayer_get_duration (JmpMplayer *self, gint64 *duration);
gboolean jmp_mplayer_get_duration_string (JmpMplayer *self, gchar **duration);
gboolean jmp_mplayer_get_position (JmpMplayer *self, gint64 *position);
gboolean jmp_mplayer_get_position_in_percentage (JmpMplayer *self, gdouble *position_percentage);
gboolean jmp_mplayer_get_position_string (JmpMplayer *self, gchar **position);
gboolean jmp_mplayer_seek (JmpMplayer *self, gint64 seek_position);
gboolean jmp_mplayer_seek_in_percentage (JmpMplayer *self, gdouble seek_position_percentage);

G_END_DECLS

#endif /* _JMP_MPLAYER */
