/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "jmp-dataprovider.h"

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <stdlib.h>
#include <errno.h>

#include "jmp-artist.h"
#include "jmp-album.h"
#include "jmp-track.h"
#include "jmp-marshal.h"

  /** TODO: we should store different base_urls for the relations we
   ** expect to use, and maybe in a different file, as some kind of
   ** configuration
   **/
#define BASE_URL "http://api.jamendo.com/get2/%s/%s/%s/?searchquery\
=%s&n=%i&pn=%i&order=searchweight_desc"

G_DEFINE_TYPE (JmpDataProvider, jmp_data_provider, G_TYPE_OBJECT)

enum {
        PROP_0,
        PROP_FORMAT,
};

#define GET_PRIVATE(o) \
        (G_TYPE_INSTANCE_GET_PRIVATE ((o), JMP_DATA_PROVIDER_TYPE, JmpDataProviderPrivate))

struct _JmpDataProviderPrivate {
        gchar *format;
        JmpRelation relation;
};

enum {
        RESPONSE_RECEIVED,
        LAST_SIGNAL
};

static guint
signals[LAST_SIGNAL] = { 0 };

static const char *
track_fields[] = {
        "id", "name", "duration", "stream", "url", NULL,
};

static const char *
artist_fields[] = {
        "name", "id", "idstr", "image", "genre", "mbgid",  "mbid",  "url", NULL,
};

static const char *
album_fields[] = {
        "name", "id", "url", "image", "duration", "genre", "dates", NULL,
};

struct VOSet {
        GType type;
        char *name;
        char *xpath;
        char **fields;
} vos[] = {
        { 0, "artist", "/data/artist", NULL },
        { 0, "album",  "/data/album",  NULL },
        { 0, "track",  "/data/track",  NULL },
};

typedef struct {
        JmpDataProvider *self;
        char *query;
} CallBackData;

static void
jmp_data_provider_get_property (GObject *object, guint property_id,
                         GValue *value, GParamSpec *pspec)
{
        JmpDataProvider *self = JMP_DATA_PROVIDER (object);

        switch (property_id) {
        case PROP_FORMAT:
                g_value_set_string (value, self->priv->format);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
jmp_data_provider_set_property (GObject *object, guint property_id,
                         const GValue *value, GParamSpec *pspec)
{
        JmpDataProvider *self = JMP_DATA_PROVIDER (object);

        switch (property_id) {
        case PROP_FORMAT:
                jmp_data_provider_set_format (self, g_value_get_string (value));
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
jmp_data_provider_finalize (GObject *object)
{
        JmpDataProvider *self = JMP_DATA_PROVIDER (object);

        g_free (self->priv->format);

        G_OBJECT_CLASS (jmp_data_provider_parent_class)->finalize (object);
}

static void
jmp_data_provider_class_init (JmpDataProviderClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        g_type_class_add_private (klass, sizeof (JmpDataProviderPrivate));

        object_class->get_property = jmp_data_provider_get_property;
        object_class->set_property = jmp_data_provider_set_property;
        object_class->finalize = jmp_data_provider_finalize;

        /** This is the pattern for the search **/
        /** The format for the response (e.g. xml, json, plain) **/
        g_object_class_install_property
                (object_class, PROP_FORMAT,
                 g_param_spec_string ("format", "Return format",
                                      "Format we require for the response",
                                      NULL,
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        signals[RESPONSE_RECEIVED] =
                        g_signal_new ("response-received", G_TYPE_FROM_CLASS (klass),
                                      G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                                      G_STRUCT_OFFSET (JmpDataProviderClass, response_received),
                                      NULL, NULL, jmp_marshal_VOID__STRING_POINTER,
                                      G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_POINTER);

}

static void
jmp_data_provider_init (JmpDataProvider *self)
{
        self->priv = GET_PRIVATE (self);
        self->priv->format = g_strdup ("xml");
        self->priv->relation = JMP_RELATION_TRACK;

        vos[JMP_RELATION_ARTIST].type = JMP_ARTIST_TYPE;
        vos[JMP_RELATION_ARTIST].fields = (char **) artist_fields;
        vos[JMP_RELATION_ALBUM].type = JMP_ALBUM_TYPE;
        vos[JMP_RELATION_ALBUM].fields = (char **) album_fields;
        vos[JMP_RELATION_TRACK].type = JMP_TRACK_TYPE;
        vos[JMP_RELATION_TRACK].fields = (char **) track_fields;
}

JmpDataProvider*
jmp_data_provider_new (void)
{
        return g_object_new (JMP_DATA_PROVIDER_TYPE, NULL);
}

void
jmp_data_provider_set_format (JmpDataProvider *self, const gchar *format)
{
        if (g_strcmp0 (format, "xml") == 0 ||
            g_strcmp0 (format, "json") == 0 ||
            g_strcmp0 (format, "plain") == 0) {
                g_free (self->priv->format);
                self->priv->format = g_strdup (format);
        }
}

inline static GObjectClass *
get_vo_class (GObject *item, JmpRelation relation)
{
        switch (relation) {
        case JMP_RELATION_ARTIST:
                return G_OBJECT_CLASS (JMP_ARTIST_GET_CLASS (item));
        case JMP_RELATION_ALBUM:
                return G_OBJECT_CLASS (JMP_ALBUM_GET_CLASS (item));
        case JMP_RELATION_TRACK:
                return G_OBJECT_CLASS (JMP_TRACK_GET_CLASS (item));
        }

        return NULL;
}

static void
item_set (GObject *item,
          const gchar *key,
          const gchar *value,
          JmpRelation relation)
{
        guint n;
        GParamSpec *param =
                g_object_class_find_property (get_vo_class (item, relation),
                                              key);

        if (!param) {
                g_message ("Property %s not found", key);
                return;
        }

        switch (param->value_type) {
        case G_TYPE_STRING:
                g_object_set (item, key, value, NULL);
                break;
        case G_TYPE_UINT:
        case G_TYPE_INT: {
                errno = 0;
                glong val = strtol (value, NULL, 0);
                if (!errno)
                        g_object_set (item, key, val, NULL);
                break;
        }
        case G_TYPE_FLOAT: {
                errno = 0;
                gdouble val = g_ascii_strtod (value, NULL);
                if (!errno)
                        g_object_set (item, key, val, NULL);
                break;
        }
        default:
                g_return_if_reached ();
        }
}

static GObject *
generate_vo (xmlNodePtr a_node, JmpRelation relation)
{
        xmlNodePtr cur_node = NULL;
        GObject *item = g_object_new (vos[relation].type, NULL);

        for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
                if (cur_node->type == XML_ELEMENT_NODE) {
                        char *value = xmlNodeGetContent(cur_node);
                        if (value) {
                                item_set (item, cur_node->name, value, relation);
                        }
                        g_free (value);
                }
        }

        return item;
}

static GList*
generate_list (xmlNodeSetPtr nodes, JmpRelation relation)
{
        GList *list = NULL;
        int i;

        for (i = 0; i < nodes->nodeNr; i++) {
                xmlNodePtr node = nodes->nodeTab[i];
                if (node->type == XML_ELEMENT_NODE) {
                        GObject *item = generate_vo (node->children, relation);
                        if (item)
                                list = g_list_prepend (list, item);
                }
        }

        if (list)
                return g_list_reverse (list);

        return NULL;
}

static GList*
parse_xml (const char *buffer, int length, JmpRelation relation)
{
        xmlDocPtr doc = xmlReadMemory (buffer, length, NULL, NULL,
                                       XML_PARSE_NOBLANKS | XML_PARSE_RECOVER);
        if (!doc)
                return NULL;

        GList *result = NULL;

        xmlXPathContextPtr context = xmlXPathNewContext (doc);

        xmlXPathObjectPtr xpath_obj =
                xmlXPathEvalExpression (vos[relation].xpath, context);
        if (!xpath_obj)
                goto bail;

        xmlNodeSetPtr nodeset = xpath_obj->nodesetval;
        if (nodeset->nodeNr > 0)
                result = generate_list (nodeset, relation);

bail:
        xmlXPathFreeObject (xpath_obj);
        xmlXPathFreeContext (context);
        xmlFreeDoc (doc);
        return result;
}

static void
process_response (SoupSession *session, SoupMessage *msg, gpointer user_data)
{
        CallBackData *cbdata = (CallBackData*) user_data;
        const char *mime;
        GList *list = NULL;

        /* sanity tests */
        if (!SOUP_STATUS_IS_SUCCESSFUL (msg->status_code))
                goto bail;

        if (msg->response_body->length <= 0)
                goto bail;

        mime = soup_message_headers_get_content_type (msg->response_headers, NULL);

        if (g_strcmp0 (cbdata->self->priv->format, "xml") == 0) {
                if (g_strcmp0 (mime, "text/xml") == 0 ||
                    g_strcmp0 (mime, "application/xml") == 0) {
                        list = parse_xml (msg->response_body->data,
                                          msg->response_body->length,
                                          cbdata->self->priv->relation);
                } else {
                        g_warning ("Didn't receive an xml document");
                }
        } else {
                g_warning ("output format parsing not implemented");
        }

bail:
        g_signal_emit (cbdata->self, signals[RESPONSE_RECEIVED], 0, cbdata->query, list);
        g_free(cbdata->query);
        g_free(cbdata);
}

static char *
get_query_uri (JmpRelation relation, const char *format, const char *query, const int n_elements, const int n_page)
{
        if (relation >= G_N_ELEMENTS (vos))
                return NULL;

        char *fields = g_strjoinv ("+", vos[relation].fields);
        char *uri = g_strdup_printf (BASE_URL, fields,
                                     vos[relation].name,
                                     format, query,
                                     n_elements, n_page);
        g_free (fields);
        return uri;
}

gboolean
jmp_data_provider_query (JmpDataProvider *self,
                         JmpRelation relation,
                         const char *query,
                         int n_elements, int n_page)
{
        CallBackData *cbdata;
        g_return_val_if_fail (IS_JMP_DATA_PROVIDER (self), FALSE);
        if (!query && !query[0])
                return FALSE;

        char *uri = get_query_uri (relation, self->priv->format, query, n_elements, n_page);
        if (uri) {
                SoupSession *session;
                SoupMessage *msg;

                self->priv->relation = relation;

                session = soup_session_async_new ();
                msg = soup_message_new ("GET", uri);
                if (msg){
                        cbdata = g_malloc(sizeof (CallBackData));
                        cbdata->self = self;
                        cbdata->query = g_strdup(query);
                        soup_session_queue_message (session, msg,
                                                    process_response, cbdata);
                } else {
                        return FALSE;
                }
                g_free (uri);
                return TRUE;
        }

        return FALSE;
}
