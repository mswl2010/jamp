/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef _JMP_MPLAYER_SERVICE
#define _JMP_MPLAYER_SERVICE

#include <glib-object.h>

G_BEGIN_DECLS

#define JMP_MPLAYER_TYPE_SERVICE jmp_mplayer_service_get_type()
#define JMP_MPLAYER_SERVICE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), JMP_MPLAYER_TYPE_SERVICE, JmpMplayerService))
#define JMP_MPLAYER_SERVICE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), JMP_MPLAYER_TYPE_SERVICE, JmpMplayerServiceClass))
#define JMP_MPLAYER_IS_SERVICE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JMP_MPLAYER_TYPE_SERVICE))
#define JMP_MPLAYER_IS_SERVICE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), JMP_MPLAYER_TYPE_SERVICE))
#define JMP_MPLAYER_SERVICE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), JMP_MPLAYER_TYPE_SERVICE, JmpMplayerServiceClass))

typedef struct _JmpMplayerServicePrivate JmpMplayerServicePrivate;

typedef struct {
        GObject parent;

        /* <private> */
        JmpMplayerServicePrivate *priv;
} JmpMplayerService;

typedef struct {
        GObjectClass parent_class;
} JmpMplayerServiceClass;

GType jmp_mplayer_service_get_type (void);
JmpMplayerService* jmp_mplayer_service_new (DBusGConnection *connection);

G_END_DECLS

#endif
