/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <dbus/dbus-glib-bindings.h>
#include "jmp-track-service.h"
#include "jmp-track.h"


G_DEFINE_TYPE (JmpTrackService, jmp_track_service, G_TYPE_OBJECT)

enum {
        PROP_0,
        PROP_DBUSGCONN,
        PROP_TRACK,
        PROP_NUMBER,
};
#define GET_PRIVATE(o)                                                  \
        (G_TYPE_INSTANCE_GET_PRIVATE ((o), JMP_TRACK_SERVICE_TYPE, JmpTrackServicePrivate))

struct _JmpTrackServicePrivate {
        DBusGConnection *connection;
        JmpTrack *track;
        guint number;
};

gboolean
jmp_track_service_get_id (JmpTrackService *self, gchar **id, GError **error)
{
        g_return_val_if_fail (IS_JMP_TRACK(self->priv->track), FALSE);

        g_object_get (self->priv->track, "id", id, NULL);

        return TRUE;
}

gboolean
jmp_track_service_get_name (JmpTrackService *self, gchar **name, GError **error)
{
        g_return_val_if_fail (IS_JMP_TRACK(self->priv->track), FALSE);

        g_object_get (self->priv->track, "name", name, NULL);

        return TRUE;
}

gboolean
jmp_track_service_get_duration (JmpTrackService *self, guint *duration,
                                GError **error)
{
        g_return_val_if_fail (IS_JMP_TRACK(self->priv->track), FALSE);

        g_object_get (self->priv->track, "duration", duration, NULL);

        return TRUE;
}

gboolean
jmp_track_service_get_stream (JmpTrackService *self, gchar **stream,
                GError **error)
{
        g_return_val_if_fail (IS_JMP_TRACK(self->priv->track), FALSE);

        g_object_get (self->priv->track, "stream", stream, NULL);

        return TRUE;
}

gboolean
jmp_track_service_get_url (JmpTrackService *self, gchar **url,
                GError **error)
{
        g_return_val_if_fail (IS_JMP_TRACK(self->priv->track), FALSE);

        g_object_get (self->priv->track, "url", url, NULL);

        return TRUE;
}

#include "jmp-track-service-glue.h"

static void
setup_dbus (JmpTrackService *self)
{
        DBusGProxy *proxy;
        guint request_name_result;
        GError *error = NULL;
        char *path;

        proxy = dbus_g_proxy_new_for_name (self->priv->connection,
                                           DBUS_SERVICE_DBUS,
                                           DBUS_PATH_DBUS,
                                           DBUS_INTERFACE_DBUS);

        if (!org_freedesktop_DBus_request_name (proxy,
                                                "org.mswl.jamp",
                                                0, &request_name_result,
                                                &error)) {
		g_warning ("Unable to register service: %s", error->message);
		g_error_free (error);
	}

        path = g_strdup_printf ("/Track/%i", self->priv->number);

        dbus_g_connection_register_g_object (self->priv->connection,
                                             path,
                                             G_OBJECT (self));

        g_free (path);
        g_object_unref (proxy);
}

static void
jmp_track_service_set_property (GObject *object, guint property_id,
                                  const GValue *value, GParamSpec *pspec)
{
        JmpTrackService *self = JMP_TRACK_SERVICE (object);

        switch (property_id) {
        case PROP_DBUSGCONN:
                if (!self->priv->connection) {
                        DBusGConnection *tmp = g_value_get_pointer (value);
                        if (tmp) {
                                self->priv->connection =
                                        dbus_g_connection_ref (tmp);
                        }
                }
                g_assert (self->priv->connection);
                break;
        case PROP_TRACK:
                if (!self->priv->track) {
                        JmpTrack *tmp = g_value_get_pointer (value);
                        if (tmp)
                                self->priv->track = tmp;
                }
                g_assert (self->priv->track);
                break;
        case PROP_NUMBER:
                self->priv->number = g_value_get_uint (value);
                if (self->priv->connection)
                        setup_dbus (self);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
jmp_track_service_finalize (GObject *object)
{
        JmpTrackService *self = JMP_TRACK_SERVICE (object);

        if (self->priv->connection) {
                dbus_g_connection_unref (self->priv->connection);
        }
        g_object_unref (self->priv->track);
        G_OBJECT_CLASS (jmp_track_service_parent_class)->finalize (object);
}

static void
jmp_track_service_class_init (JmpTrackServiceClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        g_type_class_add_private (klass, sizeof (JmpTrackServicePrivate));

        object_class->set_property = jmp_track_service_set_property;
        object_class->finalize = jmp_track_service_finalize;

        g_object_class_install_property
                (object_class, PROP_DBUSGCONN,
                 g_param_spec_pointer ("connection", "DBusGConnection",
                                       "DBus GConnection",
                                       G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));

        g_object_class_install_property
                (object_class, PROP_TRACK,
                 g_param_spec_pointer ("track", "JmpTrack",
                                       "The track object instance",
                                       G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));

        g_object_class_install_property
                (object_class, PROP_NUMBER,
                 g_param_spec_uint ("number", "track number",
                                    "Track index number", 0, 10000, 0,
                                    G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));

        dbus_g_object_type_install_info (JMP_TRACK_SERVICE_TYPE,
                                         &dbus_glib_jmp_track_service_object_info);

}

static void
jmp_track_service_init (JmpTrackService *self)
{
        self->priv = GET_PRIVATE (self);
        self->priv->track = NULL;
        self->priv->connection = NULL;
        self->priv->number = 0;
}

JmpTrackService*
jmp_track_service_new (DBusGConnection *connection, JmpTrack *track, guint number)
{
        return g_object_new (JMP_TRACK_SERVICE_TYPE,
                             "connection", connection,
			     "track", track,
			     "number", number, NULL);
}
