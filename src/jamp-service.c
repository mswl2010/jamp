/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <gst/gst.h>
#include <dbus/dbus-glib.h>
#include <glib/gi18n.h>

#include "config.h"
#include "jmp-mplayer-service.h"
#include "jmp-dataprovider-service.h"

int
main (int argc, char **argv)
{
        JmpMplayerService *player;
        JmpDataProviderService *data;
        DBusGConnection *connection;
        GOptionContext *context;
        GError *error = NULL;
        GMainLoop *loop;

        if (!g_thread_supported ())
                g_thread_init (NULL);

        context = g_option_context_new (" - JaMP DBus Service");
        g_option_context_add_group (context, gst_init_get_option_group ());
        if (!g_option_context_parse (context, &argc, &argv, &error)) {
                g_critical ("option parsing failed: %s", error->message);
                return -1;
        }

        gst_init (&argc, &argv);

        connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
        if (connection == NULL) {
                g_warning ("Unable to connect to dbus: %sn", error->message);
                g_error_free (error);
                return -1;
        }

        loop = g_main_loop_new (NULL, FALSE);
        player = jmp_mplayer_service_new (connection);
        data = jmp_data_provider_service_new (connection);

        g_main_loop_run (loop);

        g_object_unref (player);
        g_object_unref (data);

        return 0;
}
