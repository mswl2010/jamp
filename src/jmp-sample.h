/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef _JMP_SAMPLE
#define _JMP_SAMPLE

#include <glib-object.h>

G_BEGIN_DECLS

#define JMP_TYPE_SAMPLE jmp_sample_get_type()
#define JMP_SAMPLE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), JMP_TYPE_SAMPLE, JmpSample))
#define JMP_SAMPLE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), JMP_TYPE_SAMPLE, JmpSampleClass))
#define JMP_IS_SAMPLE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JMP_TYPE_SAMPLE))
#define JMP_IS_SAMPLE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), JMP_TYPE_SAMPLE))
#define JMP_SAMPLE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), JMP_TYPE_SAMPLE, JmpSampleClass))

typedef struct _JmpSamplePrivate JmpSamplePrivate;

typedef struct {
        GObject parent;

        /* <private> */
        JmpSamplePrivate *priv;
} JmpSample;

typedef struct {
        GObjectClass parent_class;
} JmpSampleClass;

GType jmp_sample_get_type (void);
JmpSample* jmp_sample_new (void);

void jmp_sample_set_name (JmpSample *self, const gchar *name);
void jmp_sample_say_hello (JmpSample *self);

G_END_DECLS

#endif /* _JMP_SAMPLE */
