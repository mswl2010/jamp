/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "jmp-track.h"

G_DEFINE_TYPE (JmpTrack, jmp_track, G_TYPE_OBJECT)

enum {
        PROP_0,
        PROP_ID,
        PROP_NAME,
        PROP_DURATION,
        PROP_STREAM,
        PROP_URL,
};

#define GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), JMP_TRACK_TYPE, JmpTrackPrivate))

struct _JmpTrackPrivate {
        gchar *id;
        gchar *name;
        guint duration;
        gchar *stream;
        gchar *url;
};

static void
jmp_track_get_property (GObject *object, guint property_id,
                         GValue *value, GParamSpec *pspec)
{
        JmpTrack *self = JMP_TRACK (object);

        switch (property_id) {
        case PROP_ID:
                g_value_set_string (value, self->priv->id);
                break;
        case PROP_NAME:
                g_value_set_string (value, self->priv->name);
                break;
         case PROP_DURATION:
                g_value_set_uint (value, self->priv->duration);
                break;
        case PROP_STREAM:
                g_value_set_string (value, self->priv->stream);
                break;
        case PROP_URL:
                g_value_set_string (value, self->priv->url);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}


static void
jmp_track_set_property (GObject *object, guint property_id,
                         const GValue *value, GParamSpec *pspec)
{
        JmpTrack *self = JMP_TRACK (object);

        switch (property_id) {
        case PROP_ID:
                jmp_track_set_id (self, g_value_get_string (value));
                break;
        case PROP_NAME:
                jmp_track_set_name (self, g_value_get_string (value));
                break;
        case PROP_DURATION:
                jmp_track_set_duration (self, g_value_get_uint (value));
                break;
        case PROP_STREAM:
                jmp_track_set_stream (self, g_value_get_string (value));
                break;
        case PROP_URL:
                jmp_track_set_url (self, g_value_get_string (value));
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
jmp_track_finalize (GObject *object)
{
        JmpTrack *self = JMP_TRACK (object);
        g_free (self->priv->id);
        g_free (self->priv->name);
        g_free (self->priv->stream);
        g_free (self->priv->url);
        G_OBJECT_CLASS (jmp_track_parent_class)->finalize (object);
}

static void
jmp_track_class_init (JmpTrackClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        g_type_class_add_private (klass, sizeof (JmpTrackClass));

        object_class->get_property = jmp_track_get_property;
        object_class->set_property = jmp_track_set_property;
        object_class->finalize = jmp_track_finalize;

        g_object_class_install_property
                (object_class, PROP_ID,
                 g_param_spec_string ("id", "Track id", "Numeric id of the track",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_NAME,
                 g_param_spec_string ("name", "Track name",
                                      "Name of the track",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_DURATION,
                 g_param_spec_uint ("duration", "Track duration",
                                    "Length of the track in seconds",
                                    0, G_MAXUINT, 0,
                                    G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_STREAM,
                 g_param_spec_string ("stream", "url of the stream",
                                      "url of the stream for the track (default:mp31)",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

        g_object_class_install_property
                (object_class, PROP_URL,
                 g_param_spec_string ("url", "Track url",
                                      "Link to the page of the track on Jamendo (lyrics)",
                                      "NULL",
                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
}

static void
jmp_track_init (JmpTrack *self)
{
        self->priv = GET_PRIVATE (self);
}

JmpTrack*
jmp_track_new (void)
{
        return g_object_new (JMP_TRACK_TYPE, NULL);
}

void
jmp_track_set_id (JmpTrack *self, const gchar *id)
{
        g_free (self->priv->id);
        self->priv->id = g_strdup (id);
}

void
jmp_track_set_name (JmpTrack *self, const gchar *name)
{
        g_free (self->priv->name);
        self->priv->name = g_strdup (name);
}

void
jmp_track_set_duration (JmpTrack *self, const guint duration)
{
        self->priv->duration = duration;
}

void
jmp_track_set_stream (JmpTrack *self, const gchar *stream)
{
        g_free (self->priv->stream);
        self->priv->stream = g_strdup (stream);
}

void
jmp_track_set_url (JmpTrack *self, const gchar *url)
{
        g_free (self->priv->url);
        self->priv->url = g_strdup (url);
}
