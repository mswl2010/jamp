/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <dbus/dbus-glib-bindings.h>

#include "jmp-mplayer-service.h"
#include "jmp-marshal.h"
#include "common-defs.h"
#include "jmp-error.h"

#include "jmp-mplayer.h"

G_DEFINE_TYPE (JmpMplayerService, jmp_mplayer_service, G_TYPE_OBJECT)

enum {
        PROP_0,
        PROP_DBUSGCONN,
};

enum {
        TICK,
        LAST_SIGNAL
};

static guint
jmp_mplayer_service_signals[LAST_SIGNAL] = { 0 };

#define GET_PRIVATE(o)                                                  \
        (G_TYPE_INSTANCE_GET_PRIVATE ((o), JMP_MPLAYER_TYPE_SERVICE, JmpMplayerServicePrivate))

struct _JmpMplayerServicePrivate {
        JmpMplayer *player;
        DBusGConnection *connection;
};

gboolean
jmp_mplayer_service_set_uri (JmpMplayerService *self, const gchar *uri,
                             GError **error)
{
        return jmp_mplayer_set_uri (self->priv->player, uri);
}

gboolean
jmp_mplayer_service_play (JmpMplayerService *self, GError **error)
{
        return jmp_mplayer_play (self->priv->player);
}

gboolean
jmp_mplayer_service_stop (JmpMplayerService *self, GError **error)
{
        return jmp_mplayer_stop (self->priv->player);
}

gboolean
jmp_mplayer_service_pause (JmpMplayerService *self, GError **error)
{
        return jmp_mplayer_pause (self->priv->player);
}

gboolean
jmp_mplayer_service_set_volume (JmpMplayerService *self, double volume,
                                GError **error)
{
        gboolean is_ok = jmp_mplayer_set_volume (self->priv->player, volume);

        if (!is_ok) {
                *error = g_error_new (JMP_ERROR,
                                      JMP_ERROR_SET_VOLUME_FAILED,
                                      "%s", "Error setting volume");
        }

        return is_ok;
}

gboolean
jmp_mplayer_service_get_volume (JmpMplayerService *self, double *volume,
                                GError **error)
{
        *volume = jmp_mplayer_get_volume (self->priv->player);
        return TRUE;
}

gboolean
jmp_mplayer_service_toggle_mute (JmpMplayerService *self, GError **error)
{
        return jmp_mplayer_toggle_mute (self->priv->player);
}

gboolean
jmp_mplayer_service_get_position (JmpMplayerService *self, gint64 *position,
                                  GError **error)
{
        return jmp_mplayer_get_position (self->priv->player, position);
}

gboolean
jmp_mplayer_service_get_position_percentage (JmpMplayerService *self, gdouble *position_percentage,
                                             GError **error)
{
        return jmp_mplayer_get_position_in_percentage (self->priv->player,
                                                       position_percentage);
}


gboolean
jmp_mplayer_service_get_duration (JmpMplayerService *self, gint64 *duration,
                                  GError **error)
{
        return jmp_mplayer_get_duration (self->priv->player, duration);
}

gboolean
jmp_mplayer_service_get_duration_string (JmpMplayerService *self, gchar **duration,
                                  GError **error)
{
        return jmp_mplayer_get_duration_string (self->priv->player, duration);
}


gboolean
jmp_mplayer_service_seek (JmpMplayerService *self, gint64 seek_position,
                          GError **error)
{
        return jmp_mplayer_seek (self->priv->player, seek_position);
}

gboolean
jmp_mplayer_service_seek_percentage (JmpMplayerService *self, gdouble seek_position_percentage,
                                     GError **error)
{
        return jmp_mplayer_seek_in_percentage (self->priv->player, seek_position_percentage);
}

#include "jmp-mplayer-service-glue.h"

static void
tick_callback (JmpMplayer *self, gint64 position, gint64 duration,
               gpointer user_data)
{
        JmpMplayerService *service = JMP_MPLAYER_SERVICE (user_data);
        g_signal_emit (service, jmp_mplayer_service_signals[TICK], 0,
                       position, duration);
}

static void
setup_dbus (JmpMplayerService *self)
{
        DBusGProxy *proxy;
        guint request_name_result;
        GError *error = NULL;

        proxy = dbus_g_proxy_new_for_name (self->priv->connection,
                                           DBUS_SERVICE_DBUS,
                                           DBUS_PATH_DBUS,
                                           DBUS_INTERFACE_DBUS);

        if (!org_freedesktop_DBus_request_name (proxy,
                                                MPLAYER_SERVICE_NAME,
                                                0, &request_name_result,
                                                &error)) {
                g_warning ("Unable to register service: %s", error->message);
                g_error_free (error);
        }

        dbus_g_connection_register_g_object (self->priv->connection,
                                             MPLAYER_SERVICE_OBJECT_PATH,
                                             G_OBJECT (self));

        g_object_unref (proxy);
}

static void
jmp_mplayer_service_set_property (GObject *object, guint property_id,
                                  const GValue *value, GParamSpec *pspec)
{
        JmpMplayerService *self = JMP_MPLAYER_SERVICE (object);

        switch (property_id) {
        case PROP_DBUSGCONN:
                if (!self->priv->connection) {
                        DBusGConnection *tmp = g_value_get_pointer (value);
                        if (tmp) {
                                self->priv->connection =
                                        dbus_g_connection_ref (tmp);
                                setup_dbus (self);
                        }
                }
                g_assert (self->priv->connection);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
jmp_mplayer_service_finalize (GObject *object)
{
        JmpMplayerService *self = JMP_MPLAYER_SERVICE (object);

        if (self->priv->connection) {
                dbus_g_connection_unref (self->priv->connection);
        }

        g_object_unref (self->priv->player);
        G_OBJECT_CLASS (jmp_mplayer_service_parent_class)->finalize (object);
}

static void
jmp_mplayer_service_class_init (JmpMplayerServiceClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        g_type_class_add_private (klass, sizeof (JmpMplayerServicePrivate));

        object_class->set_property = jmp_mplayer_service_set_property;
        object_class->finalize = jmp_mplayer_service_finalize;

        g_object_class_install_property
                (object_class, PROP_DBUSGCONN,
                 g_param_spec_pointer ("connection", "DBusGConnection",
                                       "DBus GConnection",
                                       G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));

         dbus_g_object_type_install_info (JMP_MPLAYER_TYPE_SERVICE,
                                          &dbus_glib_jmp_mplayer_service_object_info);

         jmp_mplayer_service_signals[TICK] =
                         g_signal_new ("tick",
                         G_TYPE_FROM_CLASS (klass),
                         G_SIGNAL_RUN_LAST,
                         0,
                         NULL,
                         NULL,
                         jmp_marshal_VOID__INT64_INT64,
                         G_TYPE_NONE,
                         2,
                         G_TYPE_INT64,
                         G_TYPE_INT64,
                         NULL);
}

static void
jmp_mplayer_service_init (JmpMplayerService *self)
{
        self->priv = GET_PRIVATE (self);
        self->priv->player = jmp_mplayer_new ();
        self->priv->connection = NULL;

        g_signal_connect (self->priv->player, "playback-tick",
                          G_CALLBACK (tick_callback), self);
}

JmpMplayerService*
jmp_mplayer_service_new (DBusGConnection *connection)
{
        return g_object_new (JMP_MPLAYER_TYPE_SERVICE,
                             "connection", connection, NULL);
}
