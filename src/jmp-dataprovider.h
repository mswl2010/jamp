/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef _JMP_DATA_PROVIDER
#define _JMP_DATA_PROVIDER

#include <glib-object.h>
#include <libsoup/soup.h>

G_BEGIN_DECLS

typedef enum {
        JMP_RELATION_ARTIST,
        JMP_RELATION_ALBUM,
        JMP_RELATION_TRACK,
        JMP_RELATION_END
} JmpRelation;

#define JMP_DATA_PROVIDER_TYPE jmp_data_provider_get_type()
#define JMP_DATA_PROVIDER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), JMP_DATA_PROVIDER_TYPE, JmpDataProvider))
#define JMP_DATA_PROVIDER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), JMP_DATA_PROVIDER_TYPE, JmpDataProviderClass))
#define IS_JMP_DATA_PROVIDER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JMP_DATA_PROVIDER_TYPE))
#define IS_JMP_DATA_PROVIDER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), JMP_DATA_PROVIDER_TYPE))
#define JMP_DATA_PROVIDER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), JMP_DATA_PROVIDER_TYPE, JmpDataProviderClass))

typedef struct _JmpDataProviderPrivate JmpDataProviderPrivate;

typedef struct {
        GObject parent;

        /*< private >*/
        JmpDataProviderPrivate *priv;
} JmpDataProvider;

typedef struct {
        GObjectClass parent_class;

        /*< private >*/
        void (*response_received) (JmpDataProvider *self, GList *list);
} JmpDataProviderClass;

GType jmp_data_provider_get_type (void);
JmpDataProvider* jmp_data_provider_new (void);

void jmp_data_provider_set_format (JmpDataProvider *self, const gchar *format);
gboolean jmp_data_provider_query (JmpDataProvider *self,
                                  JmpRelation relation,
                                  const char *query,
                                  int n_elements, int n_page);

G_END_DECLS

#endif /*_JMP_DATA_PROVIDER */
