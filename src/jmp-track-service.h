/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef _JMP_TRACK_SERVICE
#define _JMP_TRACK_SERVICE

#include <glib-object.h>
#include "jmp-track.h"

G_BEGIN_DECLS

#define JMP_TRACK_SERVICE_TYPE jmp_track_service_get_type()
#define JMP_TRACK_SERVICE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), JMP_TRACK_SERVICE_TYPE, JmpTrackService))
#define JMP_TRACK_SERVICE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), JMP_TRACK_SERVICE_TYPE, JmpTrackServiceClass))
#define JMP_TRACK_IS_SERVICE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JMP_TRACK_SERVICE_TYPE))
#define JMP_TRACK_IS_SERVICE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), JMP_TRACK_SERVICE_TYPE))
#define JMP_TRACK_SERVICE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), JMP_TRACK_SERVICE_TYPE, JmpTrackServiceClass))

typedef struct _JmpTrackServicePrivate JmpTrackServicePrivate;

typedef struct {
        GObject parent;

        /* <private> */
        JmpTrackServicePrivate *priv;
} JmpTrackService;

typedef struct {
        GObjectClass parent_class;

} JmpTrackServiceClass;

GType jmp_track_service_get_type (void);
JmpTrackService* jmp_track_service_new (DBusGConnection *connection,
                                        JmpTrack *track, guint number);

G_END_DECLS

#endif
