/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef _JMP_ALBUM
#define _JMP_ALBUM

#include <glib-object.h>
#include "jmp-artist.h"

G_BEGIN_DECLS

#define JMP_ALBUM_TYPE jmp_album_get_type()
#define JMP_ALBUM(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), JMP_ALBUM_TYPE, JmpAlbum))
#define JMP_ALBUM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), JMP_ALBUM_TYPE, JmpAlbumClass))
#define IS_JMP_ALBUM(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JMP_ALBUM_TYPE))
#define IS_JMP_ALBUM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), JMP_ALBUM_TYPE))
#define JMP_ALBUM_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), JMP_ALBUM_TYPE, JmpAlbumClass))

typedef struct _JmpAlbumPrivate JmpAlbumPrivate;

typedef struct {
        GObject parent;

        /* <private> */
        JmpAlbumPrivate *priv;
} JmpAlbum;

typedef struct {
        GObjectClass parent_class;
} JmpAlbumClass;

GType jmp_album_get_type (void);
JmpAlbum* jmp_album_new (void);

void jmp_album_set_id (JmpAlbum *self, const gchar *id);
void jmp_album_set_name (JmpAlbum *self, const gchar *name);
void jmp_album_set_image (JmpAlbum *self, const gchar *image);
void jmp_album_set_url (JmpAlbum *self, const gchar *url);
void jmp_album_set_duration (JmpAlbum *self, const gchar *duration);
void jmp_album_set_genre (JmpAlbum *self, const gchar *genre);
void jmp_album_set_dates (JmpAlbum *self, const gchar *year);
void jmp_album_set_artist (JmpAlbum *self, JmpArtist  *artist);


G_END_DECLS

#endif
