/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <dbus/dbus-glib-bindings.h>
#include "jmp-dataprovider-service.h"
#include "jmp-dataprovider.h"
#include "jmp-track.h"
#include "jmp-track-service.h"
#include "jmp-marshal.h"
#include <string.h>

guint global_path = 0;

G_DEFINE_TYPE (JmpDataProviderService, jmp_data_provider_service, G_TYPE_OBJECT)

enum {
        PROP_0,
        PROP_DBUSGCONN,
};

enum {
        RESPONSE_RECEIVED,
        LAST_SIGNAL
        };

static guint
signals[LAST_SIGNAL] = { 0 };

#define GET_PRIVATE(o)                                                  \
        (G_TYPE_INSTANCE_GET_PRIVATE ((o), JMP_DATA_PROVIDER_SERVICE_TYPE, JmpDataProviderServicePrivate))

struct _JmpDataProviderServicePrivate {
        JmpDataProvider *dataprovider;
        DBusGConnection *connection;
};

static track
map_track (JmpTrack *gtrack)
{
        track strack;
        g_object_get (gtrack, "id", &strack.id,
                      "name", &strack.name,
                      "duration", &strack.duration,
                      "url", &strack.url,
                      "stream", &strack.stream, NULL);
        return strack;
}

#define DBUS_STRUCT_STRING_STRING (dbus_g_type_get_struct ("GValueArray", G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INVALID))

static void
jmp_data_provider_service_process_tracks (JmpDataProvider *self,
                                          gchar *search_pattern, GList *list, gpointer user_data)
{
        JmpDataProviderService *service = JMP_DATA_PROVIDER_SERVICE (user_data);
        gchar  **paths = NULL;
        guint i = 0;
        guint n_tracks = g_list_length (list);
        GList *tmp;

        paths = g_new0 (gchar *, n_tracks + 1);

        for (tmp = list; tmp; tmp = tmp->next) {
                JmpTrack *track = JMP_TRACK (tmp->data);
                JmpTrackService *track_service =
                        jmp_track_service_new (service->priv->connection,
                                               track, global_path);
                paths[i] = g_strdup_printf ("/Track/%i", global_path);
                global_path++;
                i++;
        }
        g_list_free (list);

        paths[i] = NULL;

        g_signal_emit (service, signals[RESPONSE_RECEIVED], 0, search_pattern, n_tracks, paths);

        g_strfreev (paths);
}

gboolean
jmp_data_provider_service_query_tracks (JmpDataProviderService *self,
                                        const gchar *search,
                                        gint n_elements,
                                        gint n_page,
                                        GError **error)
{

        return jmp_data_provider_query (self->priv->dataprovider,
                                        JMP_RELATION_TRACK, search, n_elements, n_page);

}

#include "jmp-dataprovider-service-glue.h"

static void
setup_dbus (JmpDataProviderService *self)
{
        DBusGProxy *proxy;
        guint request_name_result;
        GError *error = NULL;

        proxy = dbus_g_proxy_new_for_name (self->priv->connection,
                                           DBUS_SERVICE_DBUS,
                                           DBUS_PATH_DBUS,
                                           DBUS_INTERFACE_DBUS);

        if (!org_freedesktop_DBus_request_name (proxy,
                                                "org.mswl.jamp",
                                                0, &request_name_result,
                                                &error)) {
                g_warning ("Unable to register service: %s", error->message);
                g_error_free (error);
        }

        dbus_g_connection_register_g_object (self->priv->connection,
                                             "/DataProvider",
                                             G_OBJECT (self));

        g_object_unref (proxy);
}

static void
jmp_data_provider_service_set_property (GObject *object, guint property_id,
                                  const GValue *value, GParamSpec *pspec)
{
        JmpDataProviderService *self = JMP_DATA_PROVIDER_SERVICE (object);

        switch (property_id) {
        case PROP_DBUSGCONN:
                if (!self->priv->connection) {
                        DBusGConnection *tmp = g_value_get_pointer (value);
                        if (tmp) {
                                self->priv->connection =
                                        dbus_g_connection_ref (tmp);
                                setup_dbus (self);
                        }
                }
                g_assert (self->priv->connection);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
jmp_data_provider_service_finalize (GObject *object)
{
        JmpDataProviderService *self = JMP_DATA_PROVIDER_SERVICE (object);

        if (self->priv->connection) {
                dbus_g_connection_unref (self->priv->connection);
        }
        g_object_unref (self->priv->dataprovider);
        G_OBJECT_CLASS (jmp_data_provider_service_parent_class)->finalize (object);
}

static void
jmp_data_provider_service_class_init (JmpDataProviderServiceClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        g_type_class_add_private (klass, sizeof (JmpDataProviderServicePrivate));

        object_class->set_property = jmp_data_provider_service_set_property;
        object_class->finalize = jmp_data_provider_service_finalize;

        g_object_class_install_property
                (object_class, PROP_DBUSGCONN,
                 g_param_spec_pointer ("connection", "DBusGConnection",
                                       "DBus GConnection",
                                       G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));

        dbus_g_object_type_install_info (JMP_DATA_PROVIDER_SERVICE_TYPE,
                                          &dbus_glib_jmp_data_provider_service_object_info);

        signals[RESPONSE_RECEIVED] =
                        g_signal_new ("response_received", G_TYPE_FROM_CLASS (klass),
                                      G_SIGNAL_RUN_FIRST,
                                      G_STRUCT_OFFSET (JmpDataProviderServiceClass, response_received),
                                      NULL, NULL, jmp_marshal_VOID__STRING_UINT_POINTER,
                                      G_TYPE_NONE, 3, G_TYPE_STRING, G_TYPE_UINT, G_TYPE_STRV, NULL);
}

static void
jmp_data_provider_service_init (JmpDataProviderService *self)
{
        self->priv = GET_PRIVATE (self);
        self->priv->dataprovider = jmp_data_provider_new ();
        self->priv->connection = NULL;

        g_signal_connect (self->priv->dataprovider, "response-received",
                          G_CALLBACK (jmp_data_provider_service_process_tracks),
                          self);
}

JmpDataProviderService*
jmp_data_provider_service_new (DBusGConnection *connection)
{
        return g_object_new (JMP_DATA_PROVIDER_SERVICE_TYPE,
                             "connection", connection, NULL);
}
