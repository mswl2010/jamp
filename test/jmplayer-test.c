/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <stdio.h>
#include <glib.h>
#include <gst/gst.h>
#include <signal.h>
#include <string.h>
#include <glib/gi18n.h>

#include "config.h"

#include "jmp-mplayer.h"

static gchar *uri = "/usr/share/sounds/ubuntu/stereo/desktop-login.ogg";
static guint change_volume = -1;
static guint query_playback = -1;
static guint stop_after = -1;
static GMainLoop *loop;

static GOptionEntry entries[] = {
        { "uri", 'u', 0, G_OPTION_ARG_STRING, &uri, "uri", "u" },
        { "change-volume-time", 'v', 0, G_OPTION_ARG_INT, &change_volume,
                        "Change volume time", "nanosecs" },
        { "stop-after", 's', 0, G_OPTION_ARG_INT, &stop_after,
                        "Stop after", "nanosecs" },
        { NULL }
};

JmpMplayer *jmplayer;

static gboolean
change_volume_callback (gpointer user_data)
{
        JmpMplayer *jmplayer = JMP_MPLAYER (user_data);
        gdouble current_volume;

        current_volume = jmp_mplayer_get_volume (jmplayer);
        jmp_mplayer_set_volume (jmplayer, 0.5 * current_volume);

        return TRUE;
}

static void
end_of_stream_callback (JmpMplayer *jmplayer, gpointer user_data)
{
        g_message (_("The sound has ended!"));
        g_main_loop_quit(loop);
}

static void
error_callback (JmpMplayer *jmplayer, gchar *message, gpointer user_data)
{
        g_critical (_("An error has occurred: %s"), message);
        g_main_loop_quit (loop);
}

static void
tick_callback (JmpMplayer *jmplayer, gint64 position, gint64 duration,
               gpointer user_data)
{
        g_print (_("PLAYBACK: %" G_GINT64_FORMAT "/%" G_GINT64_FORMAT "\n"),
                 position, duration);
}

static gboolean
stop_after_callback (gpointer user_data)
{
        JmpMplayer *jmplayer = JMP_MPLAYER (user_data);

        jmp_mplayer_stop (jmplayer);
        g_main_loop_quit (loop);
}

static void
play_handler (int signum)
{
        switch (signum) {
                case SIGUSR1:
                        g_message (_("Caught SIGUSR1 - Play request"));
                        jmp_mplayer_play (jmplayer);
                        break;
                case SIGUSR2:
                        g_message (_("Caught SIGUSR2 - Pause request"));
                        jmp_mplayer_pause (jmplayer);
                        break;
        }
}

static void
play_signal_setup(void)
{
          struct sigaction action;

          memset (&action, 0, sizeof (action));
          action.sa_handler = play_handler;
          sigaction (SIGUSR1, &action, NULL);
          sigaction (SIGUSR2, &action, NULL);
}

int
main (int argc, char **argv)
{
        GError *error = NULL;
        GOptionContext *context;
        GOptionGroup *gst_option_group;
        char *uritmp;

        if (!g_thread_supported ())
                g_thread_init (NULL);

        context = g_option_context_new (_(" - Tests jmp-mplayer behavior"));
        gst_option_group = gst_init_get_option_group ();
        g_option_context_add_main_entries (context, entries, NULL);
        g_option_context_add_group (context, gst_option_group);
        if (!g_option_context_parse (context, &argc, &argv, &error)) {
                g_critical (_("option parsing failed: %s"), error->message);
                return -1;
        }
        g_printf (_("JAMP Media Player Test:\n"));
        g_printf (_("Send USR1 signal to play and USR2 to pause (pid: %d)\n"), getpid());
        g_printf (_("kill -USR1 %d ====== kill -USR2 %d\n"), getpid(), getpid());

        loop = g_main_loop_new (NULL, FALSE);

        jmplayer = jmp_mplayer_new ();
        g_signal_connect (jmplayer, "end-of-stream",
                          G_CALLBACK (end_of_stream_callback), NULL);

        g_signal_connect (jmplayer, "error",
                          G_CALLBACK (error_callback), NULL);
        g_signal_connect (jmplayer, "playback-tick",
                          G_CALLBACK (tick_callback), NULL);

        play_signal_setup();

        if (!g_strrstr (uri, "://")) {
                uritmp = g_filename_to_uri(uri, NULL, &error);
                if (uritmp) {
                        uri = uritmp;
                }
        }

        jmp_mplayer_set_uri (jmplayer, uri);
        jmp_mplayer_play (jmplayer);

        if (change_volume != -1) {
                g_timeout_add (change_volume, change_volume_callback,
                                jmplayer);
        }

        if (stop_after != -1) {
                g_timeout_add (stop_after, stop_after_callback, jmplayer);
        }

        g_main_loop_run (loop);

        g_object_unref (jmplayer);
        g_option_context_free (context);
        g_main_loop_unref (loop);

        return 0;
}
