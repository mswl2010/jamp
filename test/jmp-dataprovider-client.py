#!/usr/bin/python

import sys
from traceback import print_exc

import gobject

import dbus
import dbus.mainloop.glib

loop = gobject.MainLoop()

def signal_handler(n, tracks):
    print("%d tracks found" % n);
    loop.quit()

def search(query):
    bus = dbus.SessionBus ()

    try:
        proxy = bus.get_object('org.mswl.jamp', '/DataProvider')
        proxy.connect_to_signal('response_received', signal_handler)
        reply = proxy.QueryTracks(query, 20, 1)
    except dbus.DBusException:
        print_exc()
        sys.exit(1)

    #print proxy.Introspect(dbus_interface="org.freedesktop.DBus.Introspectable")

if __name__ == '__main__':
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    search(sys.argv[1])
    loop = gobject.MainLoop()
    loop.run()
