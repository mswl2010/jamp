/*
 * Copyright (C) 2010 Igalia S.L.
 *
 * Contact: mswl-dm-2009@igalia.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <dbus/dbus-glib-bindings.h>
#include "jmp-mplayer-client-glue.h"
#include "../src/common-defs.h"

static gchar *uri = NULL;
static gdouble volume = -1;
static gint64 seek_to = -1;
static gdouble seek_to_percentage = -1;
static gboolean play = FALSE;
static gboolean stop = FALSE;
static gboolean pause = FALSE;
static gboolean get_volume = FALSE;
static gboolean toogle_mute = FALSE;
static gboolean get_position = FALSE;
static gboolean get_position_in_percentage = FALSE;
static gboolean get_duration = FALSE;
static gboolean get_duration_string = FALSE;

static GOptionEntry main_entries[] = {
        { "uri", 0, 0, G_OPTION_ARG_STRING, &uri, "uri", "u" },
        { NULL }
};

static GOptionEntry playback_entries[] = {
        { "play", 0, 0, G_OPTION_ARG_NONE, &play, "play", NULL },
        { "stop", 0, 0, G_OPTION_ARG_NONE, &stop, "stop", NULL },
        { "pause", 0, 0, G_OPTION_ARG_NONE, &pause, "pause", NULL },
        { "get-position", 0, 0, G_OPTION_ARG_NONE, &get_position, "get position", NULL },
        { "get-position-percentage", 0, 0, G_OPTION_ARG_NONE, &get_position_in_percentage, "get position in percentage", NULL },
        { "get-duration", 0, 0, G_OPTION_ARG_NONE, &get_duration, "get duration", NULL },
        { "get-duration-string", 0, 0, G_OPTION_ARG_NONE, &get_duration_string, "get duration as a string", NULL },
        { "seek-to", 0, 0, G_OPTION_ARG_INT64, &seek_to, "seek to", "nanosecs" },
        { "seek-to-percentage", 0, 0, G_OPTION_ARG_DOUBLE, &seek_to_percentage, "seek to percentage", "0-0.25-1" },
        { NULL }
};

static GOptionEntry volume_entries[] = {
        { "set-volume", 0, 0, G_OPTION_ARG_DOUBLE,&volume, "set volume", "vol" },
        { "get-volume", 0, 0, G_OPTION_ARG_NONE, &get_volume, "get volume", NULL },
        { "toggle-mute", 0, 0, G_OPTION_ARG_NONE, &toogle_mute, "toggle mute", NULL },
        { NULL }
};

int handle_input ()
{
        DBusGProxy *proxy = NULL;
        DBusGConnection *connection = NULL;
        GError *error = NULL;
        gdouble tmp_volume;
        gint64 position;
        gdouble position_in_percentage;
        gint64 duration;
        gchar *duration_string;

        connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
        proxy = dbus_g_proxy_new_for_name (connection,
					   MPLAYER_SERVICE_NAME,
					   MPLAYER_SERVICE_OBJECT_PATH,
					   MPLAYER_SERVICE_INTERFACE);

        if (connection == NULL) {
                g_warning("Unable to connect to dbus: %s", error->message);
                g_error_free (error);
                return -1;
        }

        if (uri != NULL) {
                if (!org_mswl_jamp_set_uri (proxy, uri, &error)) {
                        g_warning ("Error setting URI");
                }
        }

        if (play) {
                if (!org_mswl_jamp_play (proxy, &error)) {
                        g_warning ("Error starting playback");
                }

        } else if (stop) {
                if (!org_mswl_jamp_stop (proxy, &error)) {
                        g_warning ("Error stoping playback");
                }
        } else if (pause) {
                if (!org_mswl_jamp_pause (proxy, &error)) {
                        g_warning ("Error pausing playback");
                }
        }

        if (volume != -1) {
                if (!org_mswl_jamp_set_volume (proxy, volume, &error)) {
                        g_warning ("Error setting volume");
                }
        } else if (get_volume) {
                if (org_mswl_jamp_get_volume (proxy, &tmp_volume, &error))
                        g_print ("Volume: %f\n", tmp_volume);
        } else if (toogle_mute) {
                if (!org_mswl_jamp_toggle_mute (proxy, &error)) {
                        g_warning ("Error toggling mute");
                }
        }

        if (get_position) {
                if (!org_mswl_jamp_get_position (proxy, &position, &error)) {
                        g_warning ("Error getting position");
                } else {
                        g_print ("Current position: %" G_GINT64_FORMAT "\n", position);
                }
        }

        if (get_position_in_percentage) {
                if (!org_mswl_jamp_get_position_percentage
		    (proxy, &position_in_percentage, &error)) {
                        g_warning ("Error getting position percentage: %s",
				   error->message);
                } else {
                        g_print ("Current position percentage: %f\n",
				 position_in_percentage);
                }
        }

        if (get_duration) {
                if (!org_mswl_jamp_get_duration (proxy, &duration, &error)) {
                        g_warning ("Error getting duration");
                } else {
                        g_print ("Current duration: %" G_GINT64_FORMAT "\n", duration);
                }
        }

        if (get_duration_string) {
                if (!org_mswl_jamp_get_duration_string (proxy, &duration_string, &error)) {
                        g_warning ("Error getting duration string");
                } else {
                        g_print ("Current duration: %s\n", duration_string);
                }
        }
        if (seek_to != -1) {
                if (!org_mswl_jamp_seek (proxy, seek_to, &error)) {
                        g_warning ("Error seeking");
                }
        }

        if (seek_to_percentage != -1) {
                if (!org_mswl_jamp_seek_percentage (proxy, seek_to_percentage, &error)) {
                        g_warning ("Error seeking in percentage");
                }
        }

        g_object_unref (proxy);
}

int main (int argc, char **argv)
{
        GError *error = NULL;
        GOptionContext *context;
        GOptionGroup *playback_group;
        GOptionGroup *volume_group;
        GOptionGroup *main_group;

        g_type_init ();

        context = g_option_context_new (
                        " - Test DBus C client for Jamp mplayer");
        main_group = g_option_group_new ("main",
                        "Main options",
                        "Main options",
                        NULL,
                        NULL);
        playback_group = g_option_group_new ("playback",
                        "Playback options",
                        "Playback options",
                        NULL,
                        NULL);
        volume_group = g_option_group_new ("volume",
                        "Volume options",
                        "Volume options",
                        NULL,
                        NULL);

        g_option_group_add_entries (main_group, main_entries);
        g_option_group_add_entries (playback_group, playback_entries);
        g_option_group_add_entries (volume_group, volume_entries);

        g_option_context_add_group (context, main_group);
        g_option_context_add_group (context, playback_group);
        g_option_context_add_group (context, volume_group);

        if (!g_option_context_parse (context, &argc, &argv, &error)) {
                g_print ("option parsing failed: %s\n", error->message);
                return -1;
        }
        handle_input ();
        return 0;
}
