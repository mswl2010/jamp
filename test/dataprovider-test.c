#include <glib.h>

#include "jmp-dataprovider.h"
#include "jmp-track.h"
#include "jmp-album.h"
#include "jmp-artist.h"

static gchar *search = "shake";
static gchar *relation = "artist";
JmpRelation rel = JMP_RELATION_ARTIST;

static GOptionEntry entries[] = {
        { "search", 's', 0, G_OPTION_ARG_STRING, &search,
          "Pattern you want to search for", "s" },
        { "relation", 'r', 0, G_OPTION_ARG_STRING, &relation,
          "Relation you want to search in (e.g 'album', 'artist')", "r" },
        { NULL }
};

inline static GObjectClass *
get_vo_class (GObject *item, JmpRelation relation)
{
        switch (relation) {
        case JMP_RELATION_ARTIST:
                return G_OBJECT_CLASS (JMP_ARTIST_GET_CLASS (item));
        case JMP_RELATION_ALBUM:
                return G_OBJECT_CLASS (JMP_ALBUM_GET_CLASS (item));
        case JMP_RELATION_TRACK:
                return G_OBJECT_CLASS (JMP_TRACK_GET_CLASS (item));
        }

        return NULL;
}

static void
dump (GObject *item, JmpRelation relation)
{
        guint i, n;

        GParamSpec **params =
                g_object_class_list_properties (get_vo_class (item, relation),
                                                &n);

        g_print ("{\n");
        for (i = 0; i < n && params[i]; i++) {
                const char *key = params[i]->name;
                switch (params[i]->value_type) {
                case G_TYPE_STRING: {
                        char *value;
                        g_object_get (item, key, &value, NULL);
                        if (value) {
                                g_print ("\t%s = %s\n", key, value);
                                g_free (value);
                        }
                        break;
                }
                case G_TYPE_UINT:
                case G_TYPE_INT: {
                        glong value = 0;
                        g_object_get (item, key, &value, NULL);
                        g_print ("\t%s = %ld\n", key, value);
                        break;
                }
                case G_TYPE_FLOAT: {
                        gdouble value = 0;
                        g_object_get (item, key, &value, NULL);
                        g_print ("\t%s = %lf\n", key, value);
                        break;
                }
                default:
                        g_print ("\t%s = ???\n", key);
                }
        }
        g_print ("{\n");

        g_free (params);
}

static void
process_response(JmpDataProvider *self, GList *list, gpointer user_data)
{
        GMainLoop *loop = (GMainLoop *) user_data;

        if (list) {
                GList *tmp;
                for (tmp = list; tmp; tmp = tmp->next) {
                        char *name;
                        GObject *item = G_OBJECT (tmp->data);
                        dump (item, rel);
                        g_object_unref (item);
                }
                g_list_free (list);
        }
        g_main_loop_quit(loop);
}

int
main (int argc, char **argv)
{
        JmpDataProvider *dataProvider;
        GError *error = NULL;
        GOptionContext *context;
        GMainLoop *loop;

        context = g_option_context_new (" - Searches for an artist in Jamendo");
        g_option_context_add_main_entries (context, entries, NULL);
        if (!g_option_context_parse (context, &argc, &argv, &error)) {
                g_print ("option parsing failed: %s\n", error->message);
                return -1;
        }

        g_type_init ();

        g_thread_init(NULL);

        dataProvider = jmp_data_provider_new ();

        loop = g_main_loop_new (NULL, 0);

        g_signal_connect (dataProvider, "response-received",
                          G_CALLBACK (process_response), loop);

        if (g_strcmp0 (relation, "artist") == 0) {
                rel = JMP_RELATION_ARTIST;
        } else if (g_strcmp0 (relation, "album") == 0) {
                rel = JMP_RELATION_ALBUM;
        } else if (g_strcmp0 (relation, "track") == 0) {
                rel = JMP_RELATION_TRACK;
        } else {
                g_print ("Relation not available\n");
                return 0;
        }

        if (jmp_data_provider_query (dataProvider, rel, search, 20, 1))
                g_main_loop_run(loop);

        return 0;
}
