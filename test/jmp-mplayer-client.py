#!/usr/bin/python

import sys
from traceback import print_exc

import dbus

def play(uri):
    bus = dbus.SessionBus ()

    try:
        proxy = bus.get_object('org.mswl.jamp', '/Player')
        reply = proxy.SetUri(uri, dbus_interface = "org.mswl.jamp")
        reply = proxy.Play()
    except dbus.DBusException:
        print_exc()
        sys.exit(1)

    #print proxy.Introspect(dbus_interface="org.freedesktop.DBus.Introspectable")

if __name__ == '__main__':
    try:
        play(sys.argv[1])
    except:
        print("Missing uri argument");
        sys.exit(2)
