#!/bin/sh

set -ex

rm -rf autom4te.cache

autoreconf --force -i
intltoolize --copy --force --automake

if test x$NOCONFIGURE = x; then
    ./configure "$@"
else
    echo Skipping configure process.
fi
