# -*- coding: utf-8 -*-

###########################################################################
#    Jamp
#    Copyright (C) 2010 Igalia S.L.
#
#    Contact: mswl-dm-2009@igalia.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
###########################################################################

import dbus.mainloop.glib
from dbus.exceptions import DBusException

dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()

class PlaybackManager:

    def __init__(self, tick_cb=None):

        self._bus = bus
        self._tick_cb = tick_cb
        player_object = self._bus.get_object("org.mswl.jamp",
                                             "/Player")

        self._player_interface = dbus.Interface(player_object,
                                          dbus_interface="org.mswl.jamp")

        if self._tick_cb:
            self._player_interface.connect_to_signal("tick",
                                                     self._tick_cb)

    def set_uri(self, uri):
        self._player_interface.SetUri(uri)

    def play(self):
        self._player_interface.Play()

    def stop(self):
        self._player_interface.Stop()

    def pause(self):
        self._player_interface.Pause()

    def seek(self, value):
        self._player_interface.Seek(value)

    def set_volume(self, value):
        try:
            self._player_interface.SetVolume(value)
        except DBusException, ex:
            print ex

class QueryManager:

    def __init__(self, response_received_cb=None):

        self._bus = bus
        self._response_received_cb = response_received_cb
        provider_object = self._bus.get_object("org.mswl.jamp",
                                               "/DataProvider")

        self._provider_interface = dbus.Interface(provider_object,
                                                  dbus_interface="org.mswl.jamp.dataprovider")


        self._provider_interface.connect_to_signal("response_received",
                                                   self._response_received_signal)

    def query_tracks(self, search_term, count=20, page=0):
        self._provider_interface.QueryTracks(search_term, count, page)

    def create_track(self, track_path):
        proxy = self._bus.get_object("org.mswl.jamp", track_path)
        interface = dbus.Interface(proxy,
                                   "org.mswl.jamp.track")
        name = interface.GetName()
        duration = interface.GetDuration()
        id = interface.GetId()
        stream = interface.GetStream()
        uri = interface.GetUrl()

        return {'name': name,
                'duration' : duration,
                'id' : id,
                'stream' : stream,
                'uri' : uri,
                }

    def _response_received_signal(self, search_term, track_count, track_paths):
        tracks = []
        for track_path in track_paths:
            tracks.append(self.create_track(track_path))

        if self._response_received_cb:
            self._response_received_cb(search_term, len(tracks), tracks)
