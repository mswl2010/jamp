# -*- coding: utf-8 -*-

###########################################################################
#    Jamp
#    Copyright (C) 2010 Igalia S.L.
#
#    Contact: mswl-dm-2009@igalia.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
###########################################################################

import os
import gtk

APP_NAME = "Jamp"
COPYRIGHT = "(C) 2010 Igalia S.L"
LICENSE = """
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    """
WEB = "http://gitorious.org/mswl2010/jamp"

LOCAL_DATA_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              os.pardir,
                                              os.pardir,
                                              os.pardir,
                                              'data'))

UI_XML_FILE = os.path.join(LOCAL_DATA_DIR, 'jamp.ui')
PLAYLIST_FOLDER = os.path.join(os.getenv("HOME"), "." + APP_NAME.lower(), "playlists")
MAEMO_X_ALIGNMENT = 0.5
MAEMO_Y_ALIGNMENT = 0.5
MAEMO_X_SCALE = 0.6
MAEMO_Y_SCALE = 0.6
MAEMO_H_PADDING = 20
MAEMO_V_PADDING = 40
MAEMO_H_SPACING = 30

PLAYBACK_STATE_STOP = 0
PLAYBACK_STATE_PLAYING = 1
PLAYBACK_STATE_PAUSED = 2

icon_theme = gtk.IconTheme()
if icon_theme.has_icon('general_folder'):
    #We are in Maemo
    FOLDER_ICON = icon_theme.load_icon('general_folder', 48, 0)
    TRACK_ICON = icon_theme.load_icon('general_audio_file', 48, 0)
elif icon_theme.has_icon('stock_playlist'):
    #We have gnome-icon-theme installed
    FOLDER_ICON = icon_theme.load_icon('stock_playlist', 48, 0)
    TRACK_ICON = icon_theme.load_icon('stock_effects-sound', 48, 0)
else:
    FOLDER_ICON = None
    TRACK_ICON = None
del icon_theme
