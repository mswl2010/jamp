# -*- coding: utf-8 -*-

###########################################################################
#    Jamp
#    Copyright (C) 2010 Igalia S.L.
#
#    Contact: mswl-dm-2009@igalia.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
###########################################################################

from jampui.util import constants
from xml.etree import ElementTree

import os
import gettext

_ = gettext.gettext

TRANSLATION_DOMAIN = "jamp"
LOCALE_DIR = os.path.join(os.path.dirname(__file__), "locale")

gettext.install(TRANSLATION_DOMAIN, LOCALE_DIR)

class PlaylistManager:

    def __init__(self):

        self.playlist_list = []

        if not os.path.exists(constants.PLAYLIST_FOLDER):
            os.makedirs(constants.PLAYLIST_FOLDER)
            print "PLAYLIST FOLDER CREATED AT %s\n" % constants.PLAYLIST_FOLDER

        self._load_from_folder()

    def _load_from_folder(self):

        for file in os.listdir(constants.PLAYLIST_FOLDER):
            file_name = os.path.join(constants.PLAYLIST_FOLDER, file)
            if not os.path.isfile(file_name):
                continue
            tree = ElementTree.ElementTree()
            tree.parse(file_name)

            title = tree.findtext("title")
            if not title:
                continue
            creator = tree.findtext("creator", "Unknown")
            info = tree.findtext("info", "Unknown")
            new_playlist = Playlist(title, creator, info)
            new_playlist.file_name = file_name
            for track in tree.find("trackList").getiterator("track"):
                location = track.findtext("location")
                title = track.findtext("title")
                if not location or not title:
                    continue
                creator = track.findtext("creator", "Unknown")
                album = track.findtext("album", "Unknown")
                duration = track.findtext("duration", "0")
                image = track.findtext("image", "")
                new_track = Track(location, creator, album, title, duration, image)
                new_playlist.add_track(new_track)

            self.playlist_list.append(new_playlist)

    def new(self, title, creator="", info=""):
        new_playlist = Playlist(title)
        new_playlist.creator = creator
        new_playlist.info = info
        self.playlist_list.append(new_playlist)
        return new_playlist

    def save(self):
        for item in self.playlist_list:
            item.save()
        pass


class Track:
    def __init__(self, location, creator, album, title, duration, image):
        self.location = location
        self.creator = creator
        self.album = album
        self.title = title
        self.duration = duration
        self.image = image

    def get_info(self):
        return _("<b>%(title)s</b>\n<small><b>%(creator)s</b>    \
<b>%(album)s</b>    <b>%(duration)s</b></small>") % {'title' : self.title,
                                                     'creator' : self.creator,
                                                     'album' : self.album,
                                                     'duration' : self.duration}

    def get_image(self):
        if not self.image or self.image == "":
            return self._get_placeholder_pixbuf()
        else:
            return self.image

    def _get_placeholder_pixbuf(self):
        pixbuf = constants.TRACK_ICON
        return pixbuf

    def get_xspf_element(self):
        track = ElementTree.Element("track")
        location = ElementTree.SubElement(track, "location")
        creator = ElementTree.SubElement(track, "creator")
        album = ElementTree.SubElement(track, "album")
        title = ElementTree.SubElement(track, "title")
        duration = ElementTree.SubElement(track, "duration")
        location.text = self.location
        duration.text = self.duration
        creator.text = self.creator
        album.text = self.album
        title.text = self.title
        return track

class Playlist:

    def __init__(self, title, creator="", info=""):
        self.file_name = ""
        self.title = title
        self.creator = creator
        self.info = info
        self.track_list = []

    def add_track(self, track):
        if not track in self.track_list:
            self.track_list.append(track)

    def delete_track(self, track):
        if track in self.track_list:
            self.track_list.remove(track)

    def save(self):
        if not self.file_name:
            print "PLAYLIST WITHOUT FILENAME"
            self.file_name = self.title + ".xml"
            #TODO: check existing file
        file = open(os.path.join(constants.PLAYLIST_FOLDER, self.file_name), "w")
        file.write(self.get_xspf())
        file.close()

    def get_xspf(self):
        playlist = ElementTree.Element("playlist")
        title = ElementTree.SubElement(playlist, "title")
        title.text = self.title
        creator = ElementTree.SubElement(playlist, "creator")
        creator.text = self.creator
        info = ElementTree.SubElement(playlist, "info")
        info.text = self.info
        tracklist = ElementTree.SubElement(playlist, "trackList")
        for track in self.track_list:
            tracklist.append(track.get_xspf_element())
        return ElementTree.tostring(playlist, "UTF-8")

    def get_info(self):
        return _("<b>%(title)s</b>\n<small><b>%(creator)s</b>    \
<b>%(info)s</b>    <b>%(duration)s</b></small>") % {'title' : self.title,
                                                    'creator' : self.creator,
                                                    'info' : self.info,
                                                    'duration' : self.get_duration()}

    def get_duration(self):
        duration = 0
        try:
            for track in self.track_list:
                duration += int(track.duration)
        except:
            pass
        return duration
