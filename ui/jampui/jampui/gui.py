# -*- coding: utf-8 -*-

###########################################################################
#    Jamp
#    Copyright (C) 2010 Igalia S.L.
#
#    Contact: mswl-dm-2009@igalia.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
###########################################################################

from jampui.util import constants
from dbus.exceptions import DBusException
from jampui.util.controller import PlaybackManager, QueryManager
from jampui.playlist import playlist

import os
import pygtk
pygtk.require("2.0")
import gtk
import gobject
import gettext

_ = gettext.gettext

TRANSLATION_DOMAIN = "jamp"
LOCALE_DIR = os.path.join(os.path.dirname(__file__), "locale")

gettext.install(TRANSLATION_DOMAIN, LOCALE_DIR)

class Jamp:

    def __init__(self):

        self._playback_state = constants.PLAYBACK_STATE_STOP

        # tabs
        self._search_tabs = {}
        self._playlist_tabs = {}

        # builder
        self._builder = gtk.Builder()
        self._builder.add_from_file(constants.UI_XML_FILE)
        self._builder.connect_signals(self)

        # Playback and query manager
        try:
            self._playback_manager = PlaybackManager(tick_cb=\
                                                     self.tick_signal_handler)
            self._query_manager = QueryManager(response_received_cb=\
                                               self.dataprovider_signal_handler)
        except DBusException, ex:
            print ex
            quit()

        # playlist
        self._playlist_manager = playlist.PlaylistManager()

        # widgets
        self.window = self._builder.get_object("mainWindow")
        self._playlist_box = self._builder.get_object("playListScrolledWindow")
        self._main_hbox = self._builder.get_object("mainHBox")
        self._play_button = self._builder.get_object("play_button")
        self._search_entry = self._builder.get_object("search_Entry")
        self._song_percent = self._builder.get_object("song_percent")
        self._song_adjustment = self._builder.get_object("song_adjustment")

        self._volume_adjustment = self._builder.get_object("volume_adjustment")
        self._volume_adjustment.set_value(100);

        self._song_position = self._builder.get_object("song_position")
        self._song_duration = self._builder.get_object("song_duration")
        self._search_label = self._builder.get_object("search_label")

        self._playlist_tree_view = PlaylistTreeView()
        self._playlist_box.add(self._playlist_tree_view)
        self._playlist_tree_view.add_playlists(self._playlist_manager.playlist_list)
        self._playlist_tree_view.connect("row_activated", self.playlist_activated_cb)

        self._playlist_details_tree_view = PlaylistDetailsTreeView()
        self._playlist_details_tree_view.connect("row_activated", self.track_activated_cb)

        self._notebook = gtk.Notebook()
        self._notebook.set_scrollable(True)
        self._main_hbox.add(self._notebook)
        self._query_tracks = []
#        self._search_box.add(self._playlist_details_tree_view)

        self.window.show_all()

    def run(self):
        gtk.main()

    def tick_signal_handler(self, position, duration):
        self._song_adjustment.value = position
        self._song_adjustment.upper = duration

        self._song_position.set_text(self.time_on_nanoseconds_to_string(position))
        self._song_duration.set_text(self.time_on_nanoseconds_to_string(duration))

    def dataprovider_signal_handler(self, search_term, n_tracks, tracks):
        self._search_label.set_text(_("%(count)d tracks found with %(term)s") % \
                                   {'count':n_tracks,
                                    'term':search_term})

        search_tree_view = SearchTreeView()
        search_tree_view.connect("row_activated", self.query_search_activated_cb)

        for track in tracks:
            search_tree_view.append_track([track,
                                           track['name'],
                                           self.time_on_seconds_to_string(track['duration'])
                                           ])

        n_query_tab = self._search_tabs[search_term]
        search_window = self._notebook.get_nth_page(n_query_tab)
        search_window.remove(search_window.get_child())
        search_window.add(search_tree_view)

        self._notebook.set_current_page(n_query_tab)
        self.window.show_all()

    def set_uri(self, uri):
        self._playback_manager.set_uri(uri)
        self.sync_volume(100)

    def search(self):
        query = self._search_entry.get_text()

        if (len(query) and not self.add_search_tab(query)):
            self._query_manager.query_tracks(query, 20, 0)

    def sync_volume(self, volume):
        self._playback_manager.set_volume(volume / 100)

    def time_on_seconds_to_string(self, total_seconds):
        seconds = total_seconds % 60
        minutes = total_seconds / 60

        return _("%(minutes)d:%(seconds)02d") % {'minutes':minutes,
                                              'seconds':seconds}

    def time_on_nanoseconds_to_string(self, nanoseconds):

        seconds = nanoseconds / 1000000000

        return self.time_on_seconds_to_string(seconds)

    def add_search_tab(self, query):
        """
        Searchs for ``query'' and shows results in a tab
        If the query was already done, and the tab already existed, it is 
        shown again and the function returns False
        Otherwise, a new one is created
        """
        if (query in self._search_tabs):
            self._notebook.set_current_page(self._search_tabs[query])
            return False
        else:
            search_window = gtk.ScrolledWindow()
            search_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

            search_label = gtk.Label(_('searching %(query)s...' % {'query':query}));
            search_window.add_with_viewport(search_label)
            tablabel = gtk.Label(_('search: "%(query)s"' % {'query':query}))
            search_window.show_all()
            self._notebook.append_page(search_window, tablabel)

            n_pages = self._notebook.get_n_pages()
            self._search_tabs[query] = n_pages - 1
            self._notebook.set_current_page(n_pages - 1)
            return True

    def search_entry_activate_cb(self, entry):
        print "SEARCH ON ACTIVATE"
        self.search()

    def search_entry_icon_press_cb(self, entry, icon_pos, event):
        print "SEARCH ON ICON"
        self.search()

    def playlist_activated_cb(self, treeview, path, view_column):
        model, iter = treeview.get_selection().get_selected()
        self._playlist_details_tree_view.set_content(model.get_value(iter, 0).track_list)

        playlist_window = gtk.ScrolledWindow()
        playlist_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        playlist_window.add(self._playlist_details_tree_view)

        label = gtk.Label('playlist')
        self._notebook.append_page(playlist_window, label)
        self._notebook.show_all()
        # TODO select playlist page
        self._notebook.set_current_page(0)
        print "PLAYLIST CLICKED"

    def track_activated_cb(self, treeview, path, view_column):
        model, iter = treeview.get_selection().get_selected()
        print model.get_value(iter, 0).title
        if self._playback_state != constants.PLAYBACK_STATE_STOP:
            self.stop()
        self.set_uri(model.get_value(iter, 0).location)
        self.play()
        print "TRACK ACTIVATED"

    def query_search_activated_cb(self, treeview, path, view_column):
        model, iter = treeview.get_selection().get_selected()
        if self._playback_state != constants.PLAYBACK_STATE_STOP:
            self.stop()
        self.set_uri(model.get_value(iter, 0)['stream'])
        self.play()
        print "TRACK IN SEARCH ACTIVATED"

    def new_menu_activate_cb(self, menuitem):
        playlist_dialog = self._builder.get_object("newPlaylistDialog")
        print playlist_dialog
        playlist_title_entry = self._builder.get_object("titleEntry")
        playlist_creator_entry = self._builder.get_object("creatorEntry")
        playlist_info_entry = self._builder.get_object("infoEntry")

        result = playlist_dialog.run()
        if result == 1:
            created_playlist = self._playlist_manager.new(playlist_title_entry.get_text(),
                                                          playlist_creator_entry.get_text(),
                                                          playlist_info_entry.get_text())
            self._playlist_tree_view.add_playlist(created_playlist)
        playlist_dialog.hide()

    def about_menu_activate_cb(self, menuitem):
        About()

    def main_window_destroy_cb(self, widget):
        print "EXIT"
        if self._playback_state != constants.PLAYBACK_STATE_STOP:
            self.stop()
        self._playlist_manager.save()
        gtk.main_quit()

    def quit_menu_activate_cb(self, widget):
        print "QUIT_MENU"
        if self._playback_state != constants.PLAYBACK_STATE_STOP:
            self.stop()
        self._playlist_manager.save()
        gtk.main_quit()

    def song_percent_change_value_cb(self, range, scroll, value):
        if self._playback_state != constants.PLAYBACK_STATE_STOP:
            self._song_position.set_text(self.time_on_nanoseconds_to_string(int(value)))
            self._playback_manager.seek(value)
            return False
        else:
            range.set_value(0)
            return True

    def volume_button_value_changed_cb(self, scalebutton, value):
        self.sync_volume(value)

    def play_menu_activate_cb(self, widget):
        self.play()

    def pause_menu_activate_cb(self, widget):
        self.pause()

    def next_menu_activate_cb(self, widget):
        self.play_next()

    def prev_menu_activate_cb(self, widget):
        self.play_previous()

    def next_button_clicked_cb(self, button):
        self.play_next()

    def prev_button_clicked_cb(self, button):
        self.play_previous()

    def play_button_clicked_cb(self, button):
        model, iter = self._playlist_details_tree_view.get_selection().get_selected()
        if self._playback_state == constants.PLAYBACK_STATE_PLAYING:
            self.pause()
        elif self._playback_state == constants.PLAYBACK_STATE_PAUSED:
            self.play()
        else:
            if iter != None:
                self.play_selected_song()

    def play_previous(self):
        if self._playlist_details_tree_view.dec_cursor():
            self.stop()
            self.play_selected_song()

    def play_next(self):
        if self._playlist_details_tree_view.inc_cursor():
            self.stop()
            self.play_selected_song()

    def play_selected_song(self):
        model, list = self._playlist_details_tree_view.get_selection().get_selected_rows()
        if list == []:
            return
        iter = model.get_iter(list[len(list) - 1])
        self.set_uri(model.get_value(iter, 0).location)
        self.play()

    def play(self):
        if self._playback_state != constants.PLAYBACK_STATE_PLAYING:
            self._playback_state = constants.PLAYBACK_STATE_PLAYING
            self._play_button.set_stock_id("gtk-media-pause")
            self._playback_manager.play()

    def stop(self):
        if self._playback_state != constants.PLAYBACK_STATE_STOP:
            self._playback_state = constants.PLAYBACK_STATE_STOP
            self._play_button.set_stock_id("gtk-media-play")
            self._playback_manager.stop()

    def pause(self):
        self._playback_state = constants.PLAYBACK_STATE_PAUSED
        self._play_button.set_stock_id("gtk-media-play")
        self._playback_manager.pause()

class About(gtk.AboutDialog):

    def __init__(self, parent=None):
        super(About, self).__init__()
        self.set_transient_for(parent)
        self.set_position(gtk.WIN_POS_CENTER)
        self.set_name(constants.APP_NAME)
        self.set_copyright(constants.COPYRIGHT)
        self.set_license(constants.LICENSE)
        self.set_website(constants.WEB)
        self.connect("response", self.close)
        self.show_all()

    def close(self, widget=None, other=None):
        self.destroy()

class SearchTreeView(gtk.TreeView):

    def __init__(self):
        super(SearchTreeView, self).__init__()
        self._search_table_model = gtk.ListStore(gobject.TYPE_PYOBJECT,
                                                   gobject.TYPE_STRING,
                                                   gobject.TYPE_STRING)
        self.set_model(self._search_table_model)

        column_render = gtk.CellRendererText()

        column = gtk.TreeViewColumn(_('Song'), column_render, text=1)
        column.set_sort_column_id(1)
        self.append_column(column)

        column = gtk.TreeViewColumn(_('Duration'), column_render, text=2)
        column.set_sort_column_id(2)
        self.append_column(column)

    def append_track(self, track):
        self._search_table_model.append(track)

class PlaylistTreeView(gtk.TreeView):

    def __init__(self):
        super(PlaylistTreeView, self).__init__()
        self._playlist_table_model = gtk.ListStore(gobject.TYPE_PYOBJECT,
                                                   gobject.TYPE_STRING)
        self.set_model(self._playlist_table_model)
        name_column_render = gtk.CellRendererText()
        name_column = gtk.TreeViewColumn (_('Playlist'), name_column_render, text=1)
        self.append_column (name_column)

        self._playlist_table_model.clear()

    def add_playlist(self, playlist):
        self._playlist_table_model.append([playlist, playlist.title])

    def add_playlists(self, playlist):
        for item in playlist:
            self._playlist_table_model.append([item, item.title])

class PlaylistDetailsTreeView(gtk.TreeView):

    def __init__(self):
        super(PlaylistDetailsTreeView, self).__init__()
        self._song_table_model = gtk.ListStore(gobject.TYPE_PYOBJECT,
                                               gobject.TYPE_STRING,
                                               gobject.TYPE_STRING,
                                               gobject.TYPE_STRING)
        self.set_model(self._song_table_model)

        column_render = gtk.CellRendererText()

        column = gtk.TreeViewColumn(_('Title'), column_render, text=1)
        column.set_sort_column_id(1)
        self.append_column(column)

        column = gtk.TreeViewColumn(_('Artist'), column_render, text=2)
        column.set_sort_column_id(2)
        self.append_column(column)

        column = gtk.TreeViewColumn(_('Album'), column_render, text=3)
        column.set_sort_column_id(3)
        self.append_column(column)

    def inc_cursor(self):
        model, iter = self.get_selection().get_selected()
        self.get_selection().unselect_all()
        if iter == None:
            return False
        if self.get_model().iter_next(iter) == None:
            iter = model.get_iter_first()
            path = model.get_path(iter)
            self.set_cursor(path, None, False)
            return True
        iter_next = model.iter_next(iter)
        path = model.get_path(iter_next)
        self.set_cursor(path, None, False)
        return True

    def dec_cursor(self):
        model, iter = self.get_selection().get_selected()
        self.get_selection().unselect_all()
        if iter:
            path = model.get_path(iter)
            if path[0] == 0:
                self.set_cursor(self.get_model().iter_n_children(None) - 1,
                                None,
                                False)
                return True
            self.set_cursor(path[0] - 1, None, False)
            return True
        return False

    def set_content(self, track_list):
        self._song_table_model.clear()
        for track in track_list:
            self._song_table_model.append([track, track.title, track.creator, track.album])

if __name__ == "__main__":
    jamp = Jamp()
    jamp.run()
