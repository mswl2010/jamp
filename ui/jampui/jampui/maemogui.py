# -*- coding: utf-8 -*-

###########################################################################
#    Jamp
#    Copyright (C) 2010 Igalia S.L.
#
#    Contact: mswl-dm-2009@igalia.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
###########################################################################

from jampui.util import constants
from jampui.util.controller import QueryManager
from jampui.playlist.playlist import PlaylistManager, Track

import hildon
import pygtk
import gobject
pygtk.require('2.0')
import gtk
import gettext
import os

_ = gettext.gettext

TRANSLATION_DOMAIN = "jamp"
LOCALE_DIR = os.path.join(os.path.dirname(__file__), "locale")

gettext.install(TRANSLATION_DOMAIN, LOCALE_DIR)

class Jamp(hildon.StackableWindow):

    def __init__(self):
        super(Jamp, self).__init__()
        self.set_title("Jamp")
        self.connect('delete-event',
                     lambda widget, event: gtk.main_quit())

        self.add(self._create_contents())
        self.set_app_menu(self._create_app_menu())

        self.show_all()

    def _create_contents(self):
        contents = gtk.Alignment(constants.MAEMO_X_ALIGNMENT,
                                 constants.MAEMO_Y_ALIGNMENT,
                                 constants.MAEMO_X_SCALE,
                                 constants.MAEMO_Y_SCALE)
        contents.set_padding(constants.MAEMO_V_PADDING,
                             constants.MAEMO_V_PADDING,
                             constants.MAEMO_H_PADDING,
                             constants.MAEMO_H_PADDING)

        hbox = gtk.HBox(True, constants.MAEMO_H_SPACING)
        hbox.pack_start(self._create_content_box(_("Last played")),
                                  expand=True, fill=True)
        hbox.pack_start(self._create_content_box("Playlists",
                                                 self._playlists_clicked_cb),
                                  expand=True, fill=True)
        hbox.pack_start(self._create_content_box(_("Searches")),
                                  expand=True, fill=True)

        contents.add(hbox)
        return contents

    def _create_content_box(self, text, callback=None):
        box = gtk.VBox()

        button = gtk.Button()
        if callback:
            button.connect("clicked", callback)
        foot_label = gtk.Label()
        foot_label.set_text(text)

        box.pack_start(button, expand=True, fill=True)
        box.pack_start(foot_label, expand=False, fill=False)

        return box

    def _playlists_clicked_cb(self, button):
        PlaylistManagerWindow()

    def _search_clicked_cb(self, button):
        dialog = SearchDialog(self)
        response = dialog.run()
        if response == gtk.RESPONSE_ACCEPT:
            search_window = SearchResultsWindow()
            search_window.start_search(dialog.get_query(),
                                       dialog.get_relation())
        dialog.destroy()

    def _create_app_menu(self):
        menu = hildon.AppMenu()

        search = hildon.GtkButton(gtk.HILDON_SIZE_AUTO)
        search.connect("clicked", self._search_clicked_cb)
        search.set_label(_("Search"))

        about = hildon.GtkButton(gtk.HILDON_SIZE_AUTO)
        about.set_label(_("About"))

        menu.append(search)
        menu.append(about)
        menu.show_all()

        return menu

    def run(self):
        gtk.main()

class SearchDialog(gtk.Dialog):

    ARTIST = 0
    ALBUM = 1
    TRACK = 2

    relations = {ARTIST:_("Artist"),
                 ALBUM:_("Album"),
                 TRACK:_("Track")}

    def __init__(self, parent):
        super(SearchDialog, self).__init__()

        self.set_transient_for(parent)
        self.set_title(_("Enter search terms"))

        self.vbox.pack_start(self._create_contents(), True, False, 0)
        self.add_button(gtk.STOCK_OK, gtk.RESPONSE_ACCEPT)

        self.show_all()

    def get_query(self):
        return self._search_entry.get_text()

    def get_relation(self):
        return self._search_button.get_active()

    def _create_contents(self):
        self._search_entry = hildon.Entry(gtk.HILDON_SIZE_FINGER_HEIGHT)
        self._search_button = self._create_picker_button()

        search_contents = gtk.VBox(False, 0)

        search_contents.pack_start(self._search_entry, True, True, 0)
        search_contents.pack_start(self._search_button, True, True, 0)

        return search_contents

    def _create_picker_button(self):
        picker_button = hildon.PickerButton(gtk.HILDON_SIZE_FINGER_HEIGHT,
                                            hildon.BUTTON_ARRANGEMENT_HORIZONTAL)
        picker_button.set_title(_("Choose relation"))

        selector = hildon.TouchSelector(text=True)
        selector.set_column_selection_mode(hildon.TOUCH_SELECTOR_SELECTION_MODE_SINGLE)

        for field in (self.ARTIST, self.ALBUM, self.TRACK):
            selector.append_text(self.relations[field])

        picker_button.set_selector(selector)
        picker_button.set_active(self.TRACK)

        return picker_button

class ListViewWindow(hildon.StackableWindow):

    SHOW_PLAYLISTS = 0
    SHOW_TRACKS = 1

    def __init__(self, title, content_type):
        super(ListViewWindow, self).__init__()
        self.set_title(title)

        contents = self._create_contents(content_type)

        self.add(contents)
        self.show_all()

    def _create_contents(self, content_type):
        main_container = gtk.VBox()
        bottom_container = gtk.HBox()
        content_area = hildon.PannableArea()
        self._tree_view = SimpleTreeView(content_type)

        content_area.add(self._tree_view)
        main_container.pack_start(content_area,
                                  expand=True,
                                  fill=True)
        bottom_container.pack_end(self._create_play_box(),
                                  expand=False,
                                  fill=False)
        main_container.pack_end(bottom_container,
                                expand=False,
                                fill=False)

        return main_container

    def _create_play_box(self):
        event_box = gtk.EventBox()
        image = gtk.Image()
        image.set_from_pixbuf(gtk.IconTheme().load_icon('mediaplayer_nowplaying_indicator_pause', 24, 0))
        event_box.add(image)
        event_box.connect("button-press-event", self._event_box_pressed_cb)
        return event_box

    def _event_box_pressed_cb(self, widget, user_data):
        pass

class SearchResultsWindow(ListViewWindow):

    def __init__(self):
        super(SearchResultsWindow, self).__init__(_("Search results"),
                                                  ListViewWindow.SHOW_TRACKS)
        self.set_app_menu(self._create_app_menu())
        self._query_manager = QueryManager(self._search_completed_cb)
        self.current_page = 0
        self._previous_button.set_sensitive(False)

        self.show_all()

    def _create_app_menu(self):
        menu = hildon.AppMenu()
        edit_button = hildon.GtkButton(gtk.HILDON_SIZE_AUTO)
        edit_button.set_label(_("Edit"))
        menu.append(edit_button)

        save_button = hildon.GtkButton(gtk.HILDON_SIZE_AUTO)
        save_button.set_label(_("Save as playlist"))
        menu.append(save_button)

        self._previous_button = hildon.GtkButton(gtk.HILDON_SIZE_AUTO)
        self._previous_button.set_label(_("Previous page"))
        self._previous_button.connect('clicked', self._previous_button_cb)
        menu.append(self._previous_button)

        self._next_button = hildon.GtkButton(gtk.HILDON_SIZE_AUTO)
        self._next_button.set_label(_("Next page"))
        self._next_button.connect('clicked', self._next_button_cb)
        menu.append(self._next_button)

        menu.show_all()
        return menu

    def _next_button_cb(self, button):
        self.current_page = self.current_page + 1
        self.start_search(self.query_terms, self.query_relation, start_page=self.current_page)

    def _previous_button_cb(self, button):
        if self.current_page != 0:
            self.current_page = self.current_page - 1
            self.start_search(self.query_terms, self.query_relation, start_page=self.current_page)

    def start_search(self, query_terms, query_relation, query_count=20, start_page=0):
        hildon.hildon_gtk_window_set_progress_indicator(self, True)
        message = _("Searching %(relation)s for %(terms)s") % \
            {'relation':SearchDialog.relations[query_relation],
             'terms':query_terms}
        if start_page != 0:
            message += _(" (page %(page)d)") % {'page':start_page}

        self.query_terms = query_terms
        self.query_relation = query_relation
        self.query_count = query_count

        self._query_manager.query_tracks(query_terms,
                                         count=query_count, page=start_page)
        banner = hildon.hildon_banner_show_information(self, "", message)
        banner.set_timeout(2000)

    def _search_completed_cb(self, terms, count, tracks):
        hildon.hildon_gtk_window_set_progress_indicator(self, False)
        fake_tracks = []
        for track in tracks:
            fake_tracks.append(self._create_fake_track(track))

        self._tree_view.add_elements(fake_tracks)

        self._next_button.set_sensitive(self.query_count <= count)
        self._previous_button.set_sensitive(self.current_page != 0)

        return False

    def _create_fake_track(self, track):
        return Track(track['uri'], _("Unknown"), _("Unknown"),
                     track['name'], track['duration'], None)

class PlaylistManagerWindow(ListViewWindow):

    def __init__(self):
        super(PlaylistManagerWindow, self).__init__(_("Playlists"),
                                                    ListViewWindow.SHOW_PLAYLISTS)
        self._load_playlists()
        self.show_all()

    def _load_playlists(self):
        manager = PlaylistManager()
        self._tree_view.add_elements(manager.playlist_list)
        self._tree_view.connect('row-activated', self._row_activated_cb)

    def _row_activated_cb(self, view, path, column):
        playlist = view.get_object_from_path(path)
        PlaylistWindow(playlist)

class PlaylistWindow(ListViewWindow):

    def __init__(self, playlist):
        super(PlaylistWindow, self).__init__(playlist.title,
                                             ListViewWindow.SHOW_TRACKS)
        self._playlist = playlist
        self._load_items()
        self.show_all()

    def _load_items(self):
        self._tree_view.add_elements(self._playlist.track_list)

class SimpleTreeView(gtk.TreeView):

    def _create_columns(self, model):
        image_renderer = gtk.CellRendererPixbuf()
        column = gtk.TreeViewColumn('Image', image_renderer,
                                    pixbuf=model.IMAGE_COLUMN)
        self.append_column(column)

        text_renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn('Name', text_renderer,
            markup=model.INFO_COLUMN)
        self.append_column(column)

    def __init__(self, content_type):
        super(SimpleTreeView, self).__init__()
        model = SimpleListStore(content_type)

        self.set_model(model)
        self._create_columns(model)

        self.show_all()

    def add_elements(self, object_collection=[]):
        model = self.get_model()
        model.add(object_collection)

    def get_object_from_path(self, path):
        model = self.get_model()
        return model[path][model.VALUE_COLUMN]

class SimpleListStore(gtk.ListStore):

    IMAGE_COLUMN = 0
    INFO_COLUMN = 1
    VALUE_COLUMN = 2

    def __init__(self, content_type):
        super(SimpleListStore, self).__init__(gtk.gdk.Pixbuf,
                                         gobject.TYPE_STRING,
                                         gobject.TYPE_PYOBJECT)
        self.content_type = content_type

    def add(self, object_collection=[]):
        self.clear()
        for object in object_collection:
            row = {}

            if self.content_type == ListViewWindow.SHOW_TRACKS:
                row[self.IMAGE_COLUMN] = object.get_image()
            else:
                row[self.IMAGE_COLUMN] = constants.FOLDER_ICON
            row[self.INFO_COLUMN] = object.get_info()
            row[self.VALUE_COLUMN] = object

            self.append(row.values())

if __name__ == "__main__":
    jamp = Jamp()
    jamp.run()
